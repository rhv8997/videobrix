import sqlite3

print("\n All the videos \n")

conn = sqlite3.connect('database/database.db')
cur = conn.cursor()
data = cur.execute("SELECT * FROM videos")
for row in data:
    print(row)


print("\n table info for videos \n")

columns = cur.execute("PRAGMA table_info(videos)")
for row in columns:
    print(row)
