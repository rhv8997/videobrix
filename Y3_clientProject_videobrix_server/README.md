# VideoBrix Raspberry PI Server

Flask application that runs on Raspberry PIs to send and receive data.
The reason of the application is to enable individual raspberry PIs to
receive data over HTTP requests. Python 3 is used per client request handling per the amount of libraries available to perform
multi device communication.

This server receives data from the React application that can be found
on the repository:

https://gitlab.cs.cf.ac.uk/c1639984/videobrix

The application on the other repository provides a central solution.
Solution aimed to be deployed to a central server. (E.g. AWS, DigitalOcean) This will enable users to have central access point to various raspberry PIs connected to their account. After they select the Raspberry PI they want to manage they can start adjusting options from the website.


### Technology Justifications

*Python 3* is portable and can be installed to any platform/system.
As this application is purely developed with python 3 it will work on
any platform that has python 3. Maturity of python and amount of documentation found on python was also a benefit.

*Flask (Python 3 Framework)* is used as base framework to configure web services and accept requests over web.

*SQLite* is used as a simple database solution per device. Each device needs to store their individual data. E.g. their settings, video locations, schedules etc. Moreover, this data needs to be accessible from the Web as well as the local application. In the local application DST innovations created SQLite was used therefore, there was configuration for them already there. Moreover, python 3 supports SQLite very well and its portability and level of simplicity makes SQLite the selected tool for this job.


### Prerequisites

1. Python 3
2. pip
3. All of the modules listed for python below.


### Modules

On linux use "sudo" to install as administrator.
If you are not an administrator install using "--user".
If you have multiple versions of python 2.x and 3.x
you need to run pip3 install python 3 or configure your pip to
install for python 3.

1. pip install schedule
2. pip install Flask
3. pip install flask_cors
4. pip install sqlite3 (Might come with default python installation.)
5. pip install threading (Might come with default python installation.)


### Setup & Deployment

1. Clone this repository extract files from the ZIP file.
2. Install all the Prerequisites and Modules mentioned above.
3. Redirect a command line/terminal to the root folder of this project.
(Where the server.py is found.)
4. run python server.py
5. EXTRA: you need to run python3 server.py if you have multiple versions
of python installed on your machine.


### Testing

Currently no testing is available for this repository.


### Authors

1. Arda Karaderi
2. Batu Ellery
3. Reagan Vose
4. Daniel Duggan
5. Emma Brady


### Contributing

Please see contact with DST innovations for details.

Controllers folder contains all the logic for handling specific requests.

API.py in the routes folder contains all the server end points created to accept requests over the configured host.

Static folder contains static files served by the Flask server.
E.g. video files for video preview feature on the React UI.

Database and Databases folder contains SQLite databases.
savedsettings.db contains the last saved settings for screen adjustments.
(Gamma, contrast etc)

It also contains schedules for the videos. (In a seperate table.)

All other settings like video sources etc is recorded on database.db
in the database folder.

Server.py controls the configuration for the web sever. (Flask)
If you need to change IP address or ports or general logic
you need to edit that file.


### Licensing

No licensing available for this project.


### Acknowledgements

Each script file contains references to sources used.
