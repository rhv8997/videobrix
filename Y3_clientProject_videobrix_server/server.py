# Packages
import os
import sys
from flask import Flask, redirect, request, render_template, make_response, escape, session, jsonify
from flask_cors import CORS
from routes.api import api
import sqlite3


from controllers.scheduling import create_schedule_today, listen_schedule_queue, schedule_scheduling
import threading
import time

app = Flask(__name__)

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif','mp4']) # Accepted extensions for file uploads.

app.register_blueprint(api, url_prefix="/api")
CORS(app, resources={r"/api/*": {"origins": ["http://127.0.0.1:3000", "http://localhost:3000"]}})
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

def run_app():
    app.run(host='127.0.0.1', port=8080) # General settings like host IP and Port.

def run_scheduling():
    time.sleep(1)
    create_schedule_today()
    schedule_scheduling()
    listen_schedule_queue()

# Threading https://pymotw.com/3/threading/ 05/03/2019
app_run = threading.Thread(name='app_run', target=run_app)
sch_run = threading.Thread(name='sch_run', target=run_scheduling)

if __name__ == "__main__":
    app_run.start()
    sch_run.start()
