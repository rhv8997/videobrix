import sqlite3

conn = sqlite3.connect('database/database.db')
cur = conn.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS "videos" (
                "name"	TEXT,
                "file"	TEXT,
                "description"	TEXT,
                "imagetimer" INTEGER,
                PRIMARY KEY("file")
            )''')

cur.execute('''CREATE TABLE IF NOT EXISTS "playlists" (
                "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
                "name"	TEXT,
                "description"	TEXT,
                "image"	TEXT
            )''')

cur.execute('''CREATE TABLE IF NOT EXISTS "playlist_videos" (
                "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
                "playlist_id"	INTEGER,
                "file"	INTEGER,
                FOREIGN KEY("file") REFERENCES "videos"("file"),
                FOREIGN KEY("playlist_id") REFERENCES "playlists"("id")
            )''')


# Video name
# Video File
# Video Description
