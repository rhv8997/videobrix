from flask import Blueprint, abort, request, jsonify, render_template
from functools import wraps
import sqlite3
import json

from controllers.auth import verify_token, set_public_key as set_pub_key

from controllers.playlists import playlist_videos, playlists, playlist, create_playlist
from controllers.videos import upload_video,delete_video, get_videos, get_videoCount, get_filteredVideos, get_videoCountFiltered
from controllers.scheduling import create_schedule, get_all_schedules, delete_schedule
from controllers.support import support_submit
from controllers.screen_settings import I2C_set_brightness, I2C_set_contrast, I2C_set_saturation, I2C_set_rotation, I2C_set_red, I2C_set_green, I2C_set_blue

api = Blueprint('api', __name__)

DATABASE = './databases/savedsettings.db'

def auth(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if(request.headers.get('Authorization')):
            token = request.headers.get('Authorization')
            token = token.replace("Bearer ", "")
            auth = verify_token(token)
            if(auth['status']):
                return func(*args, **kwargs)
            else:
                return jsonify(auth)
        else:
            return jsonify({'status': False, 'message': 'No token provided'})
    return decorated_function

# Routes

@api.route('/setKey', methods=['POST'])
def setPublicKey():
    return jsonify(set_pub_key(request.data))

# Ian Cooper- Cardaiff University - CM6112 Introduction to Web Development - 2016 - Lectures 1 - 17
# Path for accepting data for changing brightness.
@api.route( '/set/brightness', methods=['GET','POST'])
def set_brightness():
   if request.method == 'POST':
      theValue = str(request.data)
      print(theValue)

      receivedData = request.data
      JSONData = json.loads(receivedData.decode("utf-8"))

      I2C_set_brightness(JSONData["brightness_level"])

      return theValue

# Path for accepting data for changing contrast.
@api.route( '/set/contrast', methods=['GET','POST'])
def set_contrast():
   if request.method == 'POST':
      theValue = str(request.data)
      print(theValue)

      receivedData = request.data
      JSONData = json.loads(receivedData.decode("utf-8"))

      I2C_set_contrast(JSONData["contrast_level"])

      return theValue

# Path for accepting data for changing contrast.
@api.route( '/set/saturation', methods=['GET','POST'])
def set_saturation():
   if request.method == 'POST':
      theValue = str(request.data)
      print(theValue)

      receivedData = request.data
      JSONData = json.loads(receivedData.decode("utf-8"))

      I2C_set_saturation(JSONData["saturation_level"])


      return theValue

# Path for accepting data for changing rgb.
@api.route( '/set/red', methods=['GET','POST'])
def set_red():
   if request.method == 'POST':
      theValue = str(request.data)
      print(theValue)

      receivedData = request.data
      JSONData = json.loads(receivedData.decode("utf-8"))

      I2C_set_red(JSONData["red_level"])

      return theValue


# Path for accepting data for changing rgb.
@api.route( '/set/green', methods=['GET','POST'])
def set_green():
   if request.method == 'POST':
      theValue = str(request.data)
      print(theValue)

      receivedData = request.data
      JSONData = json.loads(receivedData.decode("utf-8"))

      I2C_set_green(JSONData["green_level"])

      return theValue


# Path for accepting data for changing rgb.
@api.route( '/set/blue', methods=['GET','POST'])
def set_blue():
   if request.method == 'POST':
      theValue = str(request.data)
      print(theValue)

      receivedData = request.data
      JSONData = json.loads(receivedData.decode("utf-8"))

      I2C_set_blue(JSONData["blue_level"])

      return theValue

# Path for accepting data for changing rotation.
@api.route( '/set/rotation', methods=['GET','POST'])
def set_rotation():
   if request.method == 'POST':
      theValue = str(request.data)
      print(theValue)

      receivedData = request.data
      JSONData = json.loads(receivedData.decode("utf-8"))

      I2C_set_rotation(JSONData["rotation"])
      return theValue


# this code should be moved into controllers

# Path for saving settings.
@api.route( '/save/settings', methods=['POST'])
def save_settings():
   receivedData = request.data

   # https://stackoverflow.com/questions/34579327/jsondecodeerror-expecting-value-line-1-column-1/34579607
   JSONData = json.loads(receivedData.decode("utf-8"))

   conn = sqlite3.connect(DATABASE)
   cur = conn.cursor()

   information = [(JSONData["brightness"], JSONData["contrast"], JSONData["saturation"],
    JSONData["red"], JSONData["green"], JSONData["blue"], JSONData["rotation"])]
   cur.executemany("INSERT INTO settings (brightness, contrast, saturation, red, green, blue, rotation) VALUES (?, ?, ?, ?, ?, ?, ?)",information)

   conn.commit()
   conn.close()

   return "Settings Saved"


# Path for getting saved settings.
@api.route( '/lastSettings', methods=['GET','POST'])
def get_saved_settings():
   conn = sqlite3.connect(DATABASE)
   cur = conn.cursor()

   cur.execute("SELECT * FROM settings ORDER BY id DESC LIMIT 1")
   data = cur.fetchall()
   return jsonify(data)


@api.route( '/set/schedule', methods=['POST'])
def set_schedule():
    receivedData = request.data

    JSONData = json.loads(receivedData.decode("utf-8"))

    data = create_schedule(JSONData["video"], JSONData["day"], JSONData["time"])

    return jsonify(data)

@api.route( '/schedule/all', methods=['GET'])
def all_schedule():
    return get_all_schedules()

@api.route( '/delete/schedule', methods=['POST'])
def delete_a_schedule():
    receivedData = request.data

    JSONData = json.loads(receivedData.decode("utf-8"))

    return delete_schedule(JSONData["video_id"])
# End of reference to Ian Cooper.


@api.route('/playlists', methods=['GET'])
def get_all_playlists():
   return jsonify(playlists())

@api.route('/playlists/<id>', methods=['GET'])
def get_playlist(id):
   return jsonify(playlist(id))

@api.route('/playlists/<id>/videos', methods=['GET'])
def get_all_playlist_videos(id):
   return jsonify(playlist_videos(id))

@api.route('/playlists/create', methods = ['POST', 'OPTIONS'])
def get_create_playlist():
   return jsonify(create_playlist(request))

@api.route('/check', methods= ['GET'])
def check_is_senderbox():
  return jsonify({"status": True, "message":"Valid senderbox", "senderbox": True})


@api.route('/auth')
@auth
def test():
    return "kek"


@api.route('/upload',methods = ['POST', 'GET', 'OPTIONS'])
def uploadVideo():
    return jsonify(upload_video(request))

@api.route('/supportSubmit',methods = ['POST', 'GET', 'OPTIONS'])
def supportSubmit():
    print("\n hey \n")
    return jsonify(support_submit(request))

@api.route('/delete',methods = ['POST', 'GET', 'OPTIONS'])
def deleteVideo():
    return jsonify(delete_video(request))

@api.route('/allVideos', methods=['GET'])
def getVideos():
    return jsonify(get_videos())

@api.route('/allVideosCount', methods=['GET'])
def getVideoCount():
    return jsonify(get_videoCount())

@api.route('/filter',methods = ['POST', 'GET', 'OPTIONS'])
def getFilteredVideos():
    return jsonify(get_filteredVideos(request))

@api.route('/filterCount',methods = ['POST', 'GET', 'OPTIONS'])
def getFilteredVideosCount():
    return jsonify(get_videoCountFiltered(request))
