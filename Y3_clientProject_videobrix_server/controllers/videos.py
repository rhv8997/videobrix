import os
import sqlite3
import random, string
from werkzeug.utils import secure_filename

# Reference: http://flask.pocoo.org/docs/1.0/patterns/fileuploads/
# & also http://flask.pocoo.org/docs/0.12/patterns/sqlite3/
# Ian Cooper Cardiff university & Arda Karaderi 
def upload_video(request):
    msg="upload-video"
    if request.method == 'POST':
        upload = request.files['file']
        fileName = request.form['name']
        fileDescription = request.form['description']
        imagetimer = request.form['imagetimer']
        print (imagetimer)
        filename = secure_filename(upload.filename)
        # https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python reference
        name = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))
        
        # split by the . to get rid of the file extention name such as .jpg or .mp4 etc.
        split = filename.split('.')
        ext = split[len(split)-1]
        # this takes off the extention and saves it as ext 
         # upload to a file called images, using name . and the extention. 
        upload.save(os.path.join('./static/images', name + "." + ext))
        # Then the code connects to the sqlite database
        with sqlite3.connect("database/database.db") as con:
            cur = con.cursor()
            # curser is used to insert into sqlite3 daabase into videos, updating the name
            # the file and the description
            cur.execute("INSERT INTO videos (name,file,description,imagetimer) \
            VALUES (?,?,?,?)",(fileName,name + "." + ext,fileDescription,imagetimer) )
            con.commit()
            msg = "Record successfully added to sqlite3 database."
    return  msg


def delete_video(request):
    msg="ok"
    if request.method == 'POST':
        print(request.get_json())
        json = request.get_json()
        filename = json['filename']
        print(filename)
        try:
            with sqlite3.connect("database/database.db") as con:
                cur = con.cursor()
                cur.execute("DELETE FROM videos WHERE file=?", (filename,))
                con.commit()
                msg = "Record successfully deleted from SQLite"
                if os.path.exists('./static/images'):
                    print(filename)
                    os.remove(os.path.join('./static/images',filename))
                    print("Deleted from images and database")
                else:
                    print("The file does not exist")
        except Exception as e:
            print(e)

    return msg

    # --- End of Reference: ---

    #Path for selecting all videos
def get_videos():
   with sqlite3.connect("database/database.db") as con:
      cur = con.cursor()
      cur.execute("SELECT name, file, description  FROM videos")
      vids = cur.fetchall();
   return vids



   #Path for selecting all videos count
def get_videoCount():
   with sqlite3.connect("database/database.db") as con:
      cur = con.cursor()
      cur.execute("SELECT COUNT(name) FROM videos")
      count = cur.fetchall();
   return count


      #Path for selecting all videos based on filter
def get_filteredVideos(request):
   msg="ok"
   if request.method == 'POST':
      searchItem = request.form['searchItem']
      with sqlite3.connect("database/database.db") as con:
         cur = con.cursor()
         cur.execute("SELECT * FROM videos WHERE name LIKE '%"+searchItem+"%' OR description LIKE '%"+searchItem+"%'")
         filterdVids = cur.fetchall()

   return filterdVids

     #Path for selecting all videos count
def get_videoCountFiltered(request):
   msg="ok"
   if request.method == 'POST':
      searchItem = request.form['searchItem']
   with sqlite3.connect("database/database.db") as con:
      cur = con.cursor()
      cur.execute("SELECT COUNT(name) FROM videos WHERE name LIKE '%"+searchItem+"%' OR description LIKE '%"+searchItem+"%'")
      filteredCount = cur.fetchall();
   return filteredCount

def support_submit(request):
    msg="ok"
    status = "Creation couldn't start"

    try:
        if request.method == 'POST':
            # customerName = request.form['customerName']
            # customerEmail = request.form['customerEmail']
            # customerMessage = request.form['customerMessage']
            # today = str(date.today())

            customerName = "testing my code actually works"
            customerEmail = "its probably wont bc it never does"
            customerMessage = "help me pls"



            with sqlite3.connect("database/database.db") as con:
                cur = con.cursor()
                cur.execute("INSERT INTO support_tickets (customer_name,customer_email,customer_message) \
                VALUES (?,?,?)",(customerName,customerEmail,customerMessage) )
                con.commit()
                msg = "Record successfully added"
    except:
        status = "Creation failed due to DB connection error."
        print("Error occoured in DB connection")
    return  status
