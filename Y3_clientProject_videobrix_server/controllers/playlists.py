import os
import sqlite3
import random, string
import base64
from werkzeug.utils import secure_filename


def playlist_videos(id):
    print('ol')
    try:
        with sqlite3.connect("database/database.db") as con:
            cur = con.cursor()
            cur.execute('''SELECT videos.name, videos.file, videos.description, playlists.name  FROM playlist_videos
            INNER JOIN videos ON videos.file = playlist_videos.file
            INNER JOIN playlists ON playlists.id = playlist_videos.playlist_id
            WHERE playlists.id=(?)''', (id,))
            videos = cur.fetchall()
            videoDict = []
            print(videos)
            for video in videos:
                #needs changing when thumbnails ? are implemented
                videoDict.insert(len(videoDict), {"name": video[0], "file": video[1], "description": video[2], "playlist_name": video[3]})
            return  {"status": True, "videos": videoDict}
    except Exception as e:
        print(e)

def playlists():
    try:
        with sqlite3.connect("database/database.db") as con:
            cur = con.cursor()
            cur.execute('''SELECT * FROM playlists''')
            playlists = cur.fetchall()
            playlistDict = []
            for playlist in playlists:
                playlistDict.insert(len(playlistDict), {"id": playlist[0], "name": playlist[1], "description": playlist[2], "image": playlist[3]})
            return {"status": True, "playlists": playlistDict}
    except Exception as e:
        print(e)

def playlist(id):
    try:
        with sqlite3.connect("database/database.db") as con:
            cur = con.cursor()
            cur.execute('''SELECT * FROM playlists WHERE id=?''', (id,))
            playlist = cur.fetchone()
            playlistDict = {"id": playlist[0], "name": playlist[1], "description": playlist[2], "image": playlist[3]}
            return  {"status": True, "playlist": playlistDict}
    except Exception as e:
        print(e)

def create_playlist(request):
    try:
        image = request.files['image']
        name = request.form['name']
        videos = request.form.getlist('videos')
        description = request.form['description']
        filename = secure_filename(image.filename)
        hashname = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))
        split = filename.split('.')
        ext = split[len(split)-1]
        image.save(os.path.join('static/images', hashname + "." + ext))

        with sqlite3.connect("database/database.db") as con:
            cur = con.cursor()
            cur.execute("INSERT INTO playlists (name, description, image) \
            VALUES (?,?,?)",(name , description, hashname + "." + ext) )

            playlist_id = cur.lastrowid
            db = []
            for video in videos:
                db.insert(len(db), (playlist_id, video))
            con.executemany('INSERT INTO playlist_videos (playlist_id, file) VALUES(?, ?)', db)
            con.commit()
            return {"status": True, "message": "Playlist created succesfully", "playlist_id": playlist_id}
    except Exception as e:
        return {"status": False, "message": e}
    
    return '123'