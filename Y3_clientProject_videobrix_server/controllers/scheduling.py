import datetime
import sqlite3
import json
import schedule
import time
import re
from flask import jsonify
import os

DATABASE = './databases/savedsettings.db'
days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]

# REF: Sheduling library used. Accessed on: 05/03/2019
# https://schedule.readthedocs.io/en/stable/
# REF: SQLite3 logic https://docs.python.org/3/library/sqlite3.html ACCESSED ON: 06/03/2019

def create_schedule(video, day, time): # Creates the record for a schedule.
    # Checking the date format and inserting it to DB.
    # REF of regex checks
    # regex logic https://stackoverflow.com/questions/12595051/check-if-string-matches-pattern ACCESSED ON: 06/03/2019
    # regex pattern https://stackoverflow.com/questions/7536755/regular-expression-for-matching-hhmm-time-format ACCESSED ON: 06/03/2019 BY Niket Pathak

    status = "Creation couldn't start"

    if day.lower() in days:
        try:
            conn = sqlite3.connect(DATABASE)
            cur = conn.cursor()

            cur.execute("INSERT INTO schedules (day, time, video) VALUES (?, ?, ?)", (day.lower(), time, video,))

            conn.commit()
            conn.close()

            status = "Schedule created."
        except:
            status = "Creation failed due to DB connection error."
            print("Error occoured in DB connection")

    currentDate = datetime.datetime.today()
    currentDay = currentDate.weekday()
    currentDayStr = days[currentDay]

    if day.lower() == currentDayStr: # Adds video to queue if the video meant to be played today.
        print(video + " will play at " + time)
        schedule.every().day.at(time).do(play_video, video=video).tag('task-today')

    return status

# REF bASH COMMAND FOR VIDEO: Client's documentation on how to play videos on raspberry PI.
def play_video(video): # Sends the bash command to play the video.
    video_location = "./static/images/" + video
    os.system("screen -dmS mainDisplay sh -c 'omxplayer --dbus_name BigScreen --display 5 \"" + str(video_location) + "\"; exit; exec bash'")
    print(video_location + " should play now.")


def get_all_schedules(): # Lists all schedules
    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()
        cur.execute("SELECT * FROM schedules")

        data = cur.fetchall()

        # REF: JSONIFY: https://kite.com/python/docs/flask.json.jsonify ACCESSED ON: 10/03/2019
        return jsonify(data)
    except:
        print("Error occoured in DB connection")

        return "Fetching Failed"




def create_schedule_today(): # Recreates the schedule for the new day.
    schedule.clear('task-today')

    # Fetching list of videos scheduled for today.
    currentDate = datetime.datetime.today()
    currentDay = currentDate.weekday()
    currentDayStr = days[currentDay]

    print(currentDayStr)

    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()

        cur.execute("SELECT * FROM schedules WHERE day = ?", (currentDayStr,))

        data = cur.fetchall()
        print("Schedule for " + days[currentDay])
        # REF: https://schedule.readthedocs.io/en/stable/faq.html#how-can-i-cancel-several-jobs-at-once ACCESSED ON 26/03/2019.
        # REF: SQLite3 logic https://docs.python.org/3/library/sqlite3.html ACCESSED ON: 06/03/2019
        for row in data: # Creates a schedule for every video assigned to today.
            print(row[3] + " will play at " + row[2])
            schedule.every().day.at(row[2]).do(play_video, video=row[3]).tag('task-today')

    except:
        print("Error occoured in DB connection")

    return "Schedule for today is created"


def schedule_scheduling(): # Schedules creationg of schedule for the new day.
    print("Rescheduling will happen at: 00:00")
    schedule.every().day.at("00:00").do(create_schedule_today)


def delete_schedule(video_id): # delete_schedule the record for a schedule.

    status = "Deletion couldn't start"

    try:
        conn = sqlite3.connect(DATABASE)
        cur = conn.cursor()

        cur.execute("DELETE FROM schedules WHERE id = ?", (video_id,))

        conn.commit()
        conn.close()

        status = "Schedule deleted."

    except:
        status = "Deletion failed due to DB connection error."
        print("Error occoured in DB connection")

    create_schedule_today()

    return status


# REF: How keyboard interrupt handled. http://zguide.zeromq.org/py:interrupt 20/03/2019
def listen_schedule_queue(): # Listening the schedule queue for upcoming videos.
    print("Listening the queue of schedules.")
    flag = True
    try:
        while flag == True:
            schedule.run_pending()
            time.sleep(3)

    except KeyboardInterrupt:
        print("W: interrupt received, stopping…")
