import jwt
import os
import sys
import json
from pathlib import Path
from datetime import datetime  
from datetime import timedelta  

mainDir = Path(__file__).parents[1]

def verify_token(token):
    try:
        public = open(os.path.join(mainDir, "key.pub"), "r").read()
        jwt.decode(token, public, algorithms=['RS256'])
    except jwt.exceptions.InvalidTokenError as e:
        return {'status': False, 'message': 'Token malformed'}
    except FileNotFoundError as e:
        return {'status': False, 'message': 'No public key has been set for this senderbox, please set one!'}
    return {'status': True, 'message': 'Token valid'}

def set_public_key(data):
    try:
        keyExists = open(os.path.join(mainDir, 'key.pub'), 'r')
    except FileNotFoundError:
        data = json.loads(data.decode("utf8"))
        try:
            pubKey = data['publickey']
            pubKeyFile = open(os.path.join(mainDir, 'key.pub'), 'w+')
            pubKeyFile.write(pubKey)
            pubKeyFile.close()
            return {'status': True, 'message': 'Public key has been set succesfully!'}
        except AttributeError:
            return {'status': False, 'message': 'You have not sent a public key'}
    except Exception:
        print(Exception)
    return {'status': False, 'message': 'This senderbox has already been claimed! '}
    #make way to override this

# expire = datetime.now() + timedelta(minutes=10)
# huh = jwt.encode({'some': 'payload', 'exp': expire}, private, algorithm='RS256').decode('utf-8')
# gen token for future use