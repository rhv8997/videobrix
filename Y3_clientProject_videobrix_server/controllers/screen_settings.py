import os


# Regular options REF: Client's exisiting application's code
# BASH COMMANDS ARE From application's documentation.
def I2C_set_brightness(value):
    os.system("i2cset -y 1 0x3c 0x00 " + str(value) + " &")
    return True


def I2C_set_contrast(value):
    os.system("i2cset -y 1 0x3c 0x01 " + str(value) + " &")
    return True


def I2C_set_saturation(value):
    os.system("i2cset -y 1 0x3c 0x02 " + str(value) + " &")
    return True


def I2C_set_rotation(value):
    os.system("i2cset -y 1 0x3c 0x03 " + str(value) + " &")
    return True


# Gamma options
def I2C_set_red(value):
    os.system("i2cset -y 1 0x3c 0x0A " + str(value) + " &")
    return True


def I2C_set_green(value):
    os.system("i2cset -y 1 0x3c 0x0B " + str(value) + " &")
    return True


def I2C_set_blue(value):
    os.system("i2cset -y 1 0x3c 0x0C " + str(value) + " &")
    return True
