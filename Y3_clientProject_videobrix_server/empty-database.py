import sqlite3


con_1 = sqlite3.connect('database/database.db')
con_2 =  sqlite3.connect('databases/savedsettings.db')
cur_1 = con_1.cursor()
cur_2 = con_2.cursor()


cur_1.execute('''DELETE FROM videos''')
cur_1.execute('''DELETE FROM playlists''')
cur_1.execute('''DELETE FROM playlist_videos''')

cur_2.execute('''DELETE FROM schedules''')
cur_2.execute('''DELETE FROM settings''')

con_1.commit()
con_2.commit()
