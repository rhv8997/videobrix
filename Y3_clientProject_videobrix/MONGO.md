
### Setup mongo and Compass
 1. Download MongoDB server from https://www.mongodb.com/download-center/community (under the server tab)

2. Run the downloaded install file, and when prompted install compass, do so.

### Connecting to a server

1. Open up mongo DB Compass

2. Leave all the fields as default

3. Click the connect button, and if the mongo server has been setup properly it should connect

### Data import

This import data guide should work for any collection, so feel free to extend it to other data that needs importing

1. Create a database 

2. For the name of the database, preferably use the one set in the Database.setup command’s second parameter on line 13 in the file /backend/index.js (Currently set to videobrix)

3. For the collection name, chose one of the collections as described below

4. Go to mongojson folder in this repostitory and download/copy the users.json file.

5. For each of the JSON Files in there, create a new collection in the newly created database. This can be done by clicking the plus arrow that appears when you hover over the database name. The name of the collection should be identical to the file names in the collections folder in teams, so if the file name is users.json’ name the collection users

6. Open the collection by clicking on its name on the left bar (if it isn’t visible collapse the database by clicking on the chevron arrow)

7. You should now have all the collections you need, to import data into each individual one:

	a. Click on the collection

	b. Download the appropriate collection json file from the folder

	c. You should see the taskbar update, which should now contain a ‘Collection’ button, click it then click on import data

	d. Leave the input file type

	e. Browse for the JSON file that you downloaded and click import

PS – The current username and password combination is admin and 123, the password is hashed for security reasons hence the massive string.