# Videobrix

VideoBrix is a product owned by DST innovations. VideoBrix are small displays
that are connected together to from a big display. This project aims to develop
a web user interface for these displays. For frontend work project uses
React. React is served on an Express server. For the backend, project project uses
Flask which is stored in another repository. (https://gitlab.cs.cf.ac.uk/c1645238/videobrix-raspberry-pi-server).
Communications between both servers are made with AJAX Post requests.

### Technology Justifications

*React* is used to make reactive changes on the system, as there is lots of controls its not ideal to refresh page
when a new request is made from the dashboards to the server.

*Express* (Node.js framework) is used as React needs to be hosted on a central server that will be accessible to everyone.

*MongoDB* is used to controll access to the central system. (Authorisation to the UI)

*Flask* is used to accept requests. Flask servers are deployed on each raspberry pi. Flask is chosen as
it is very easy to setup and edit. Also, *python* is very well developed in bash communications.
Maturity of python, simplicity of Flask and requirement of running bash commands to communicate with
the FPGA board is considered to make this decision.

### Prerequisites

1. Node.js
2. NPM (Comes as an Extra to Node.js)
3. MongoDB (See x for setting up the mock DB)
4. Python 3 (Flask as a library)


### Setup & Deployment

1. Clone repository via SSH/Download Zip
2. Run ```npm install``` to install all packages
3. Either run ```npm run buildDev``` to run and watch all react files or ```npm run build``` for a one time  build
4. Run ```npm run createKeys``` to create keys to authenticate user logins (this may take some time)
5. Run ```npm run start``` to start the express server
6. Load up ```localhost:3000``` on any browser

If you need to set up the mongodb server please refer to the MONGO.MD file


#### Setting up with Flask Server

1. Clone repository from https://gitlab.cs.cf.ac.uk/c1645238/videobrix-raspberry-pi-server to the Raspberry PI.
2. Start the Flask server by running bash command "python server.py"
3. Server should start on ```localhost:8080```
4. EXTRA: Port can be changed by editing the server.py file. (Commented line at the end of the script shows the IP and the port.)


#### Communications with Flask Server

1. React is used for the data input.
2. For each react component there is a data handling method.
3. Data handling method is used to take the user input.
4. Data handling method then is used for creating AJAX requests to flask server.
5. For each type of input there is a flask server endpoint.
6. Reqests are sent to those end points.
7. When flask receives the requests, it produces a bash command to apply requested change on the FPGA Board.
8. Or it runs the sql commands to apply changes to the playlist DBs.


### Testing

There are component level and snapshot level testing configured for this project.

Tests for each component can be found under frontend>components. For files with tests configured
you can see a file named .test.js. so if there is example.js the component test for that file is
is example.test.js. Locations of each test file is not given as they can be found on multiple locations.

To run the tests and check the status of the project. You need to Setup the project first.
Then you need to build the project with the instructions provided above.
After installing the dependencies and building the project you can run "npm test"
to run jest which finds all the .test.js files in the project root and runs them.

You need to wait a bit to see the results once the tests are run you will be prompted with
success rate of the project and if a test fails you will be displayed the expected and received outputs of the
test you will also see the location of the test file so that you can easily find it.

Jest is used as the core testing module for this project as it has functionality to render components in detail and test them.

Basic testing functionality that comes with react is also used in the tests written so benefits of
both modules are used. Snapshot testing is completely done by react core testing module.
Their results are also displayed with the "npm test" command so no extra commands should be run for them.


### Authors

1. Arda Karaderi
2. Batu Ellery
3. Reagan Vose
4. Daniel Duggan
5. Emma Brady


### Contributing

Please see contact with DST innovations for details.


### Licensing

No licensing available for this project.


### Acknowledgements

Each script/javascript file contains references to sources used.
