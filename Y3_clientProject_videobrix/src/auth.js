const axios = require('axios')
const authCheck = () => {
  let jwt = localStorage.getItem('jwt')
  return new Promise((resolve, reject) => {
    // if (!jwt) {
    //   reject(new Error('Invalid auth'))
    // } else {
    resolve(axios.post('/api/auth', { token: jwt }))
    // }
  })
}

module.exports = {
  authCheck
}
