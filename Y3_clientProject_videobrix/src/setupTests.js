// REF: SETUP TESTS FILE IS BASED ON DOCS
// https://airbnb.io/enzyme/docs/installation/
// Accessed on 18/02/2019.
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })
// End of referenced code
