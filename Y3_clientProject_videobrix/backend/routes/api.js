const express = require('express')
const router = express.Router()

const AuthController = require('../controllers/auth')
const PlaylistController = require('../controllers/playlist')
const SenderboxController = require('../controllers/senderbox')
const ManageController = require('../controllers/manage')

let manage = new ManageController()
let auth = new AuthController()
let playlist = new PlaylistController()
let senderbox = new SenderboxController()

const getTokenFromHeader = req => {
  let token =
    req.headers['authorization'] ||
    req.body.token ||
    req.query.token ||
    req.headers['x-access-token']
  if (req.headers['authorization']) {
    token = token.replace('Bearer ', '')
  }
  return token
}

router.get('/create', function (req, res, next) {
  auth.genRSA()
})
router.get('/keys', (req, res, next) => {
  return auth
    .getKeys(getTokenFromHeader(req))
    .then(keys => {
      return res.json({
        status: true,
        auth: true,
        keys: keys
      })
    })
    .catch(err => {
      return res.json({
        status: true,
        auth: false,
        err: err.message
      })
    })
})

router.get('/createJWT', (req, res, next) => {
  auth
    .createJWT('pywyz7j13lj')
    .then(jwt => {
      return res.json({
        status: true,
        token: jwt
      })
    })
    .catch(err => {
      return res.json({
        status: false,
        err: err.message
      })
    })
})

router.get('/setPublicKey', (req, res, next) => {
  auth
    .createJWT('pywyz7j13lj')
    .then(jwt => {
      return res.json({
        status: true,
        token: jwt
      })
    })
    .catch(err => {
      return res.json({
        status: false,
        err: err.message
      })
    })
})

router.get('/createPublicKey', (req, res, next) => {
  let pubkey = auth.genPublicKey()
  return res.json({
    status: true,
    token: pubkey
  })
})

router.get('/playlists', function (req, res, next) {
  auth
    .checkJWTPromise(getTokenFromHeader(req))
    .then(decoded => {
      playlist
        .getPlaylists(decoded.userid)
        .then(playlists => {
          res.send(
            JSON.stringify({
              status: true,
              playlists: playlists
            })
          )
        })
        .catch(err => {
          res.send(
            JSON.stringify({
              status: false,
              message: err.message
            })
          )
        })
    })
    .catch(err => {
      res.send(
        JSON.stringify({
          status: true,
          auth: false,
          message: err.message
        })
      )
    })
})

router.get('/keys', function (req, res, next) {
  return auth.genRSA()
})

router.post('/login', async (req, res, next) => {
  let key = auth.login(req.body.username, req.body.password)
  return key.then(data => {
    if (data) {
      return res.json({
        status: true,
        auth: true,
        token: data
      })
    } else {
      return res.json({
        status: false,
        auth: false
      })
    }
  })
})

router.use(function (req, res, next) {
  let token = getTokenFromHeader(req)
  auth
    .checkJWT(token)
    .then(decoded => {
      next()
    })
    .catch(e => {
      return res.json({
        status: true,
        auth: false,
        message: 'Failed to authenticate.',
        error: e
      })
    })
})

router.get('/connect/:id', (req, res, next) => {
  auth.checkJWTPromise(getTokenFromHeader(req)).then(decoded => {
    senderbox
      .connect(decoded.userid, req.params.id)
      .then(token => {
        if (token) {
          res.send(
            JSON.stringify({
              status: true,
              auth: true,
              token: token
            })
          )
        } else {
          return res.json({
            status: false,
            auth: false
          })
        }
      })
      .catch(err => {
        res.send(
          JSON.stringify({
            status: true,
            auth: false,
            message: err.message
          })
        )
      })
  })
})

router.post('/addSenderbox', (req, res, next) => {
  auth
    .checkJWTPromise(getTokenFromHeader(req))
    .then(decoded => {
      senderbox
        .addSenderbox(decoded.userid, req.body.ip, req.body.key, req.body.name)
        .then(message => {
          res.send(
            JSON.stringify({
              status: true,
              message: message
            })
          )
        })
        .catch(err => {
          res.send(
            JSON.stringify({
              status: false,
              message: err.message
            })
          )
        })
    })
    .catch(err => {
      res.send(
        JSON.stringify({
          status: false,
          auth: false,
          message: err.message
        })
      )
    })
})

router.post('/auth', function (req, res, next) {
  if (auth.checkJWT(req.body.token)) {
    res.send(
      JSON.stringify({
        status: true,
        auth: true
      })
    )
  } else {
    res.send(
      JSON.stringify({
        status: false,
        auth: false
      })
    )
  }
})

const AuthorisedGetRoute = (route, callback) => {
  router.get(route, (req, res, next) => {
    auth
      .checkJWTPromise(getTokenFromHeader(req))
      .then(decoded => {
        callback(decoded).then(data => {
          res.send(data)
        })
      })
      .catch(err => {
        res.send(
          JSON.stringify({
            status: false,
            auth: false,
            message: err.message
          })
        )
      })
  })
}

AuthorisedGetRoute('/userinfo', authInfo => {
  return auth
    .getUserInfo(authInfo.userid)
    .then(user => {
      return JSON.stringify({
        status: true,
        user
      })
    })
    .catch(err => {
      return JSON.stringify({
        status: false,
        message: err.message
      })
    })
})

router.get('/senderboxes', (req, res, next) => {
  auth
    .checkJWTPromise(getTokenFromHeader(req))
    .then(decoded => {
      senderbox
        .getSenderboxes(decoded.userid)
        .then(senderboxes => {
          res.send(
            JSON.stringify({
              status: true,
              senderboxes
            })
          )
        })
        .catch(err => {
          res.send(
            JSON.stringify({
              status: false,
              message: err.message
            })
          )
        })
    })
    .catch(err => {
      res.send(
        JSON.stringify({
          status: false,
          auth: false,
          message: err.message
        })
      )
    })
})

router.get('/playlists', function (req, res, next) {
  auth
    .checkJWTPromise(getTokenFromHeader(req))
    .then(decoded => {
      playlist
        .getPlaylists(decoded.userid)
        .then(playlists => {
          res.send(
            JSON.stringify({
              status: true,
              playlists: playlists
            })
          )
        })
        .catch(err => {
          res.send(
            JSON.stringify({
              status: false,
              message: err.message
            })
          )
        })
    })
    .catch(err => {
      res.send(
        JSON.stringify({
          status: true,
          auth: false,
          message: err.message
        })
      )
    })
})

router.get('/playlist/:id', function (req, res, next) {
  auth
    .checkJWTPromise(getTokenFromHeader(req))
    .then(decoded => {
      playlist
        .getPlaylist(req.params.id)
        .then(playlist => {
          res.send(
            JSON.stringify({
              status: true,
              playlist: playlist
            })
          )
        })
        .catch(err => {
          res.send(
            JSON.stringify({
              status: false,
              auth: true,
              message: err.message
            })
          )
        })
    })
    .catch(err => {
      res.send(
        JSON.stringify({
          status: true,
          auth: false,
          message: err.message
        })
      )
    })
})

router.get('/playlist/:playlistId/remove/:videoId', function (req, res, next) {
  auth
    .checkJWTPromise(getTokenFromHeader(req))
    .then(decoded => {
      const { playlistId, videoId } = req.params
      playlist
        .removeVideo(playlistId, videoId, decoded.userid)
        .then(() => {
          res.send(
            JSON.stringify({
              status: true,
              message: 'Video removed'
            })
          )
        })
        .catch(err => {
          res.send(
            JSON.stringify({
              status: false,
              auth: true,
              message: err.message
            })
          )
        })
    })
    .catch(err => {
      res.send(
        JSON.stringify({
          status: true,
          auth: false,
          message: err.message
        })
      )
    })
})

router.get('/playlist/:playlistId/add/:videoId', function (req, res, next) {
  auth
    .checkJWTPromise(getTokenFromHeader(req))
    .then(decoded => {
      playlist
        .getPlaylist(req.params.id)
        .then(playlist => {
          res.send(
            JSON.stringify({
              status: true,
              playlist: playlist
            })
          )
        })
        .catch(err => {
          res.send(
            JSON.stringify({
              status: false,
              auth: true,
              message: err.message
            })
          )
        })
    })
    .catch(err => {
      res.send(
        JSON.stringify({
          status: true,
          auth: false,
          message: err.message
        })
      )
    })
})

router.get('/videos', function (req, res, next) {
  auth
    .checkJWTPromise(getTokenFromHeader(req))
    .then(decoded => {
      playlist
        .getPlaylist(req.params.id)
        .then(playlist => {
          res.send(
            JSON.stringify({
              status: true,
              playlist: playlist
            })
          )
        })
        .catch(err => {
          res.send(
            JSON.stringify({
              status: false,
              message: err.message
            })
          )
        })
    })
    .catch(err => {
      res.send(
        JSON.stringify({
          status: true,
          auth: false,
          message: err.message
        })
      )
    })
})

// Reference: Code adaped from Batu's Login feature from above
router.post('/changePassword', async (req, res, next) => {
  let key = auth.login(req.body.username, req.body.password)
  return key.then(data => {
    if (data) {
      return auth.checkJWTPromise(data).then(decoded => {
        if (decoded) {
          return manage.changePassword(decoded.userid, req.body.newPassword).then(() => {
            return res.json({
              status: true,
              auth: true,
              token: data
            })
          })
        } else {
          return res.json({
            status: false,
            auth: false
          })
        }
      })
    }
    //end of reference
  })
})

module.exports = router
