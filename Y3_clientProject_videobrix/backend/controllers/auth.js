// Imports
const jwt = require('jsonwebtoken')
const fs = require('fs')
const path = require('path')
const bcrypt = require('bcryptjs')
const db = require('../src/database')
const nodersa = require('node-rsa')


// Helper funciton to open a file, helps code re use and has a try catch so that the code doesnt crash if an error occurs
// Takes in a path and uses the synchronous read file function from FS, and sets the output encoding to UTF8 since it defaults to a buffer
const openFile = path => {
  try {
    return fs.readFileSync(path, { encoding: 'utf8' })
  } catch (e) {
    console.log(e)
  }
}

const cert = openFile(path.join(__dirname, '/../certs/key.pem'))
const pub = openFile(path.join(__dirname, '/../certs/cert.pem'))


class auth {
  // Verify that the given token against the supplied public token, using the RS256 algorithm. The public token is generated form the private one
  // and is reused across the senderboxes pi
  async checkJWT(token) {
    let d = await jwt.verify(token, pub, { algorithms: ['RS256'] })
    console.log(d)
    return d
  }

  // Check JWT Function as above but wrapped with a promise, a bit redundant considering using ASYNC and AWAIT wraps code in promises anyway, resolve
  // and rejects with more concise errors to help the user
  checkJWTPromise(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, pub, { algorithms: ['RS256'] }, (err, decoded) => {
        if (err) {
          reject(new Error('Token verification failed, jwt could be malformed.'))
        } else {
          resolve(decoded)
        }
      })
    })
  }

  // Creates a JWT after being given a fiile path to a certificate, this is a bit hardcoded since it only allows for creation of RS256 tokens however it is
  //  wrapped around a promise so you have the benefit of clearly debugging i and seeing the errors. It looks in the backend/certs/ folder for the given key name
  createJWT(filename) {
    return new Promise((resolve, reject) => {
      fs.readFile(
        path.join(__dirname, '/../certs/', filename + '.key'),
        (err, file) => {
          if (err) {
            reject(err)
          }
          resolve(file)
        }
      )
    }).then(file => {
      return jwt.sign({ userid: '1' }, file, { algorithm: 'RS256' })
    })
  }
  //
  createJWTFromPrivate(certname) {
    let file = new Promise((resolve, reject) => {
      fs.readFile(path.join(__dirname, '/../certs/', certname), (err, file) => {
        if (err) {
          reject(err)
        }
        resolve(file)
      })
    })
    return file.then(file => {
      return jwt.sign({ userid: '1' }, file)
    })
  }
  // Verifies the jWT then returns the the userid if valid, takes a token parameter that is a JWT. Returns the userid that is in the JWT payload. 
  async getUserId(token) {
    let d = await jwt.verify(token, cert)
    return d.userid
  }
  // getUserInfo function takes ID and queries the users collection for a document. Uses the database library and only returns the user info, which is why 
  // password, keys and playlist key names are  0, this is a MONGODB Projection that makes it so that the keys with 0 are not supplied.
  getUserInfo(id) {
    return db
      .findDocumentWithKeys(
        '_id',
        db.getObjectID(id),
        { password: 0, keys: 0, playlists: 0 },
        'users'
      )
      .then(user => {
        if (!user) {
          return Promise.reject(new Error('No user with given ID'))
        } else {
          return user
        }
      })
  }
  // Login function that takes a username and password, this function queries the mongodb instance users collection y the username, and if it exists
  // it will use the bcrypt library to  compare the given passowrd, otherwise it will return 'invalid credentials\
  // If the users details are valid it will return a signed JWT token so that the user can access the APIs
  login(username, password) {
    return db
      .findDocument('username', username, 'users')
      .then(user => {
        if (!user) {
          Promise.reject(new Error('Incorrect credentials'))
        } else {
          return { auth: bcrypt.compare(password, user.password), user: user }
        }
      })
      .then(result => {
        return result.auth.then(valid => {
          if (valid) {
            return jwt.sign({ userid: result.user._id }, cert, { algorithm: 'RS256' })
          } else {
            Promise.reject(new Error('Incorrect credentials'))
          }
        })
      })
      .then(token => {
        return token
      })
      .catch(e => {
        console.log(e)
      })
  }
  // Generic generate public key function using the nodersa library, key is created using 2048 bits and the public key from that private is returned.
  genPublicKey() {
    const rsa = new nodersa({ b: 2048 })
    return rsa.exportKey('public')
  }
  // Generate RSA function that takes a name parameter, this function creates a 2048 bit RSA key, generates a random name for it then saves both the 
  // private and public key from the pair in backend/certs with the appropriate names. It then creates a document to insert into the mongodb instance
  // This is put into the keys collection so that the users can use the keys to authorize themselves with the pi servers.
  async genRSA(name = '') {
    const rsa = new nodersa({ b: 2048 })
    let filename = Math.random()
      .toString(36)
      .replace('0.', '') // Generate random alphanumeric for filename
    return new Promise((resolve, reject) => {
      fs.writeFile(
        path.join(__dirname, '/../certs/', filename + '.key'),
        rsa.exportKey(),
        err => {
          if (err) {
            reject(err)
          }
          resolve()
        }
      )
    }) // Write private key contents to filename
      .then(() => {
        return new Promise((resolve, reject) => {
          fs.writeFile(
            path.join(__dirname, '/../certs/', filename + '.pub'),
            rsa.exportKey('public'),
            err => {
              if (err) {
                reject(err)
              }
              resolve()
            }
          )
        })
      })
      .then(() => {
        let object = { filename: filename, name }
        return db.insert(object, 'keys')
      })
      .then(key => {
        return key.ops[0]._id
      })
  }

  // Takes a token parameter that queries the mongodb database for all of the keys that the givne tokens related data (Userid) has.
  getKeys(token) {
    let decoded = jwt.decode(token)
    return db
      .findDocument('_id', db.getObjectID(decoded.userid), 'users')
      .then(user => {
        if (!user) {
          return Promise.reject(new Error('No user found matching that token data'))
        } else if (!user.keys) {
          return Promise.reject(new Error('No keys attached to user'))
        } else {
          let keys = user.keys.map(key => db.getObjectID(key))
          return db.findMany('_id', keys, 'keys')
        }
      })
      .then(keys => {
        return keys
      })

  }
}
module.exports = auth
