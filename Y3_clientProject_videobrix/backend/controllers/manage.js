const db = require('../src/database')
const bcrypt = require('bcryptjs')

class manage {
  addVideo() { }

  // Reference: Code adaped from Batu's Login feature on auth controller
  changePassword(id, password) {
    return db
      .findDocument('_id', db.getObjectID(id), 'users')
      .then(user => {

        if (!user) {
          return Promise.reject(new Error('User does not exist'))

        } else {
          return bcrypt.hash(password, bcrypt.genSaltSync()).then(hash => {
            user.password = hash
            console.log(hash)
            return db.update('_id', db.getObjectID(user._id), user, 'users')
          })
        }
      })
      .catch(e => {
        console.log(e)
      })
    //end of reference to batu
  }
}
module.exports = manage
