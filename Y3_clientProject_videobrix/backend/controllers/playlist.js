const db = require('../src/database')

class playlist {
  addVideo () {

  }
  removeVideo (playlistid, videoid, userid) {
    return db.findDocument('_id', db.getObjectID(userid), 'users').then((user, reject) => {
      if (user) {
        return user.playlists
      } else {
        return Promise.reject(new Error('You have no playlists'))
      }
    }).then(playlists => {
      playlists = playlists.map(playlist => String(playlist))
      if (!playlists.includes(String(playlistid))) {
        return Promise.reject(new Error('The playlist ID supplied is not one of yours, or does not exist.'))
      } else {
        return db.findDocument('_id', db.getObjectID(playlistid), 'playlists')
      }
    }).then(playlist => {
      if (!playlist.videos) {
        return Promise.reject(new Error('The playlist supplied has no videos.'))
      } else {
        let videos = playlist.videos.map(video => String(video))
        let index = videos.indexOf(String(videoid))
        if (index === -1) {
          return Promise.reject(new Error('The playlist supplied does not have any videos with the ID supplied.'))
        } else {
          videos.splice(index, 1)
        }
        playlist.videos = videos
        db.update('_id', db.getObjectID(playlist._id), playlist, 'playlists')
      }
    })
  }
  getPlaylists (userid) {
    return db.findDocument('_id', db.getObjectID(userid), 'users').then((user, reject) => {
      if (user) {
        return user.playlists
      } else {
        return Promise.reject(new Error('No users found'))
      }
    }).then(playlists => {
      playlists = playlists.map(playlist => db.getObjectID(playlist))
      return db.findMany('_id', playlists, 'playlists')
    })
  }
  getPlaylist (id) {
    return db.findDocument('_id', db.getObjectID(id), 'playlists').then((playlist) => {
      if (playlist) {
        return playlist
      } else {
        return Promise.reject(new Error('Playlist with supplied ID not found'))
      }
    }).then(playlist => {
      if (!playlist.videos) { return Promise.reject(new Error('Playlist has no videos')) }
      if (playlist.videos.lenth === 0) { return Promise.reject(new Error('Playlist has no videos')) }
      let videos = playlist.videos.map(video => String(db.getObjectID(video)))
      return db.findDocument('_id', db.getObjectID(playlist.location), 'locations').then(location => {
        let v = []
        location.videos.forEach(video => {
          if (videos.includes(String(video._id))) {
            v.push(video)
          }
        })
        playlist.videos = v
        return playlist
      })
    })
  }
}
module.exports = playlist
