const db = require('../src/database')
const axios = require('axios')
const path = require('path')
const fs = require('fs')
const jwt = require('jsonwebtoken')
const AuthController = require('./auth')

let auth = new AuthController()

class senderbox {
  async addSenderbox (userid, ip, key, name) {
    if (key === 'newkeygen') {
      key = await auth.genRSA()
    }
    return db.findDocument('_id', db.getObjectID(key), 'keys').then(key => {
      console.log(key)
      if (!key) {
        return Promise.reject(new Error('Key ID is invalid'))
      } else {
        return new Promise((resolve, reject) => {
          fs.readFile(path.join(__dirname, '/../certs/', key.filename + '.pub'), 'utf8', (err, file) => { if (err) { reject(err) } resolve(file) })
        })
      }
    })
      .then(pubkey => {
        // flask port may need to be dynamic
        return axios.post('http://' + ip + ':8080' + '/api/setKey', { publickey: pubkey })
      }).then(response => {
        console.log(userid)
        if (response.data.status) {
        // db.insert({ip: ip, key: key}, 'senderboxes')
          console.log(userid)
          return db.findDocument('_id', db.getObjectID(userid), 'users').then(user => {
            ip = `${ip}:8080`
            if (user.senderboxes) {
              user.senderboxes.push({ ip, key, name })
            } else {
              user.senderboxes = [{ ip, key, name }]
            }
            if (user.keys) {
              user.keys.push(key)
            } else {
              user.keys = [key]
            }
            console.log(user)
            db.update('_id', db.getObjectID(userid), user, 'users')
          }).then(() => {
            return response.data.message
          })
        } else {
          return Promise.reject(new Error(response.data.message))
        }
      })
  }
  getSenderboxes (userid) {
    return db.findDocument('_id', db.getObjectID(userid), 'users').then(user => {
      if (!user.senderboxes) {
        return Promise.reject(new Error('User has no senderboxes'))
      } else {
        return user.senderboxes
      }
    })
  }
  connect (userid, senderboxID) {
    return db.findDocument('_id', db.getObjectID(userid), 'users').then(user => {
      if (!user.senderboxes) {
        return Promise.reject(new Error('User has no senderboxes'))
      } else {
        let senderbox = null
        console.log(user.senderboxes)
        console.log(senderboxID)
        user.senderboxes.forEach(userSenderbox => {
          if (String(userSenderbox.name) === String(senderboxID)) {
            senderbox = userSenderbox
          }
        })
        if (senderbox === null) {
          return Promise.reject(new Error('User does not have that senderbox'))
        } else {
          return db.findDocument('_id', db.getObjectID(senderbox.key), 'keys').then(k => {
            console.log(k)
            return new Promise((resolve, reject) => {
              fs.readFile(path.join(__dirname, '/../certs', k.filename + '.key'), (err, file) => { if (err) { reject(err) }resolve(file) })
            }).then(key => {
              return jwt.sign({ 'ip': senderbox.ip, id: senderbox.id, name: senderbox.name }, key, { algorithm: 'RS256' })
            })
          })
        }
      }
    })
  }
}
module.exports = senderbox
