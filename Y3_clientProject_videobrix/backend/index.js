#!/usr/bin/env node

const express = require('express')
const http = require('http')
const path = require('path')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const Database = require('./src/database')
const cors = require('cors');
const Chatkit = require('@pusher/chatkit-server');

// File where chatkit instance locator and chatkit secret key are stored
require('dotenv').config({ path: './instances.env' });


// Routers
const indexRouter = require('./routes/index')
const apiRouter = require('./routes/api')

Database.setup('mongodb://localhost:27017', 'videobrix')

// Prepare express
const app = express()
const server = http.Server(app)

// View engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// Processors
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, '../public')))
app.use(cors());


app.use('/api', apiRouter)
app.use('/', indexRouter)

Database.connect().then(() => {
  server.listen(3000)
})

// Listen
server.on('listening', () => console.log('Example app listening on port 3000'))
server.on('error', error => {
  // Check if the error was a listening error
  if (error.syscall !== 'listen') {
    throw error
  }
  // Handle common error codes
  switch (error.code) {
    case 'EACCES':
      console.error('Port 3000 requires elevated privileges')
      process.exit(1)
    case 'EADDRINUSE':
      console.error('Port 3000 is already in use')
      process.exit(1)
    default:
      throw error
  }
})

// live chat chatkit

// REFERENCES: Folowed tutorial by Ayooluwa Isaiah from Pusher (https://pusher.com/tutorials/customer-service-chat-react)
// ACCESSED: 27/03/2019
// Made changes to styling, file names, deleted dialog file and functions, Client chat loads on page load, client name is set at dev time

// Allows server to connect to chatkit and creates new users
const chatkit = new Chatkit.default({
  instanceLocator: process.env.CHATKIT_INSTANCE_LOCATOR,
  key: process.env.CHATKIT_SECRET_KEY,
});

    app.post('/users', (req, res) => {
      const { userId } = req.body;
      chatkit
        .createUser({
          id: userId,
          name: userId,
        })
        .then(() => {
          res.sendStatus(201);
        })
        .catch(err => {
          if (err.error === 'services/chatkit/user_already_exists') {
            console.log(`User already exists: ${userId}`);
            res.sendStatus(200);
          } else {
            res.status(err.status).json(err);
          }
        });
    });
// Authenticates connection between server and chatkit
    app.post('/authenticate', (req, res) => {
      const authData = chatkit.authenticate({
        userId: req.query.user_id,
      });
      res.status(authData.status).send(authData.body);
    });

// EOF
