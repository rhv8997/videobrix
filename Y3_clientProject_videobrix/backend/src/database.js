const mongo = require('mongodb').MongoClient
const ObjectID = require('mongodb').ObjectID
const MongoNetworkError = require('mongodb').MongoNetworkError

class Database {
  static setup (url, dbname) {
    this.url = url
    this.mongo = null
    this.db = null
    this.dbname = dbname
  }
  // Connect
  static connect () {
    return new Promise((resolve, reject) => {
      mongo.connect(this.url, { useNewUrlParser: true })
        .then((client) => {
          this.mongo = client
          this.db = client.db(this.dbname)
          resolve(this)
        })
        .catch(err => {
          if (err instanceof MongoNetworkError) {
            console.error('Failed to connect to Mongo DB server')
            process.exit(1)
          }
        })
    })
  }
  // Update
  static async update (key, match, replacement, collection) {
    await this.db.collection(collection).updateOne({ [key]: match }, { $set: { ...replacement } })
    return true
  }
  static arrayUpdate (key, match, newItem, collection) {
    this.db.collection(collection).updateOne({ [key]: match }, { $push: newItem })
  }
  // Insert
  static insertSync (data, collection) {
    console.log(data)
    this.db.collection(collection).insertOne(data, (err) => {
      console.log(err)
    })
  }
  static insertMany (data, collection) {
    this.db.collection(collection).insertMany(data, (err) => {
      console.log(err)
    })
  }

  // async
  static async insert (data, collection, cb = () => {}) {
    return new Promise((resolve, reject) => {
      this.db.collection(collection).insertOne(data, (err, res) => {
        if (err) {
          reject(err)
        }
        resolve(res)
      })
    })
  }

  // Find

  static async findDocument (key, name, collection) {
    return this.db.collection(collection).findOne({ [key]: name })
  }
  static async findDocumentWithKeys (key, name, keys, collection) {
    return this.db.collection(collection).findOne({ [key]: name }, { projection: { ...keys } })
  }
  static async findRaw (filter, collection) {
    return this.db.collection(collection).findOne(filter)
  }
  static async findAll (collection) {
    return this.db.collection(collection).find({}).toArray()
  }
  static async findMultipleSearch (key, name, collection) {
    return this.db.collection(collection).find({ [key]: name }).toArray()
  }
  static async findMany (key, array, collection) {
    return this.db.collection(collection).find({ [key]: { $in: array } }).toArray()
  }
  static async lookup (from, local, foreign, asData, collection) {
    console.log(collection)
    return this.db.collection(collection).aggregate([
      { $lookup:
        {
          from: from,
          localField: local,
          foreignField: foreign,
          as: asData
        }
      }]).toArray()
  }
  static async lookupSingle (from, local, foreign, asData, key, match, collection) {
    return this.db.collection(collection).aggregate([
      { $match: { [key]: match } },
      { $lookup:
        {
          from: from,
          localField: local,
          foreignField: foreign,
          as: asData
        }
      }
    ]).toArray()
  }
  static async raw (cb) {
    return cb(this.db, this.collection)
  }
  static getObjectID (id) {
    try {
      return new ObjectID(id)
    } catch (e) {
      return 'Error: ID Supplied must be a 12 byte string or 24 hex characters'
    }
  }
  static genObjectID () {
    return new ObjectID()
  }
}
module.exports = Database
