// Imports
import React from "react";
import { Route, Switch } from "react-router-dom";
import jwt from "jsonwebtoken";

// React components
import ErrorNotFound from "./pages/error/ErrorNotFound";
import PrivateRoute from "./pages/login/PrivateRoute";

import Dashboard from "./pages/dashboard/Dashboard";

import Playlists from "./pages/playlist/Playlists";
import Playlist from "./pages/playlist/Playlist";
import CreatePlaylist from "./pages/playlist/CreatePlaylist";

import Upload from "./pages/fileupload/Upload";
import ScreenAdjustments from "./pages/adjustments/ScreenAdjustments";
import VideoControl from "./pages/VideoControl";
import AddSenderbox from "./pages/senderbox/AddSenderbox";
import Senderboxes from "./pages/senderbox/Senderboxes";
import Login from "./pages/login/Login";
import VideoLibrary from "./pages/videos/VideoLibrary";
import TopBar from "./pages/layout/TopBar";
import Schedule from "./pages/schedule/Schedule";
import ManageSchedules from "./pages/manageSchedules/ManageSchedules";
import ClientSide from "./pages/support/clientSide/ClientSide";
import TechSide from "./pages/support/techSide/TechSide";
import AccountManagement from "./pages/manageAccount/AccountManagement";

// Style
import "./app.sass";
import axios from "axios";
import LocationContext from "./locationContext";

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			locations: null,
			location: null,
			loaded: false
		};
		this.setLocation = this.setLocation.bind(this);
	}
	componentWillMount() {
		let token = localStorage.getItem("jwt");
		let curLocation = localStorage.getItem("senderboxjwt");
		let decoded = jwt.decode(curLocation);
		axios
			.get(`/api/senderboxes`, { headers: { Authorization: "Bearer " + token } })
			.then(resp => {
				if (resp.data.status) {
					return resp.data.senderboxes;
				} else {
					return null;
				}
			})
			.then(locations => {
				if (!locations) {
					this.setState({ loaded: true });
				} else {
					this.setState({
						auth: token || null,
						locations,
						location: decoded ? decoded.id : [],
						loaded: true
					});
					// hmm
				}
			});
	}

	setLocation(id) {
		this.setState({
			location: id
		});
	}
	render() {
		let token = localStorage.getItem("jwt");
		const { location, loaded } = this.state;
		console.log(location);
		return (

			<React.Fragment>
				{loaded ? (
					<React.Fragment>
						{token ? <TopBar changeLocation={this.setLocation} /> : null}
						<LocationContext.Provider value={location}>
							<Switch>
								<PrivateRoute exact path="/playlists" component={Playlists} />
								<PrivateRoute path="/playlist/create" component={CreatePlaylist} />
								<PrivateRoute exact path="/playlist/:id" component={Playlist} />
								<PrivateRoute path="/senderboxes/add" component={AddSenderbox} />
								<PrivateRoute path="/senderboxes" component={Senderboxes} />
								<PrivateRoute path="/dashboard" component={Dashboard} />
								<PrivateRoute path="/adjustments" component={ScreenAdjustments} />
								<PrivateRoute path="/dashboard" component={Dashboard} />
								<PrivateRoute path="/videos-upload" component={Upload} />
								<PrivateRoute path="/schedule/:videoTitle" component={Schedule} />
								<PrivateRoute path="/videos" component={VideoLibrary} />
								<PrivateRoute path="/adjustments" component={ScreenAdjustments} />
								<PrivateRoute path="/manageSchedules" component={ManageSchedules} />
								<PrivateRoute path="/support" component={ClientSide} />
								<PrivateRoute path="/tech" component={TechSide} />
								<PrivateRoute path="/changepassword" component={AccountManagement} />

								<Route path="/login" component={Login} />
								<Route component={ErrorNotFound} />
							</Switch>
						</LocationContext.Provider>
					</React.Fragment>
				) : null}
			</React.Fragment>

		);
	}
}
