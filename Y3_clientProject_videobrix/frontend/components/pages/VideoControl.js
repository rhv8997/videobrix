import React, { Component } from 'react'
import '../css/VideoControl.css'
import RangeSelection from '../forms/RangeSelection'

class VideoControl extends Component {
  constructor (props) {
    super(props)

    this.state = {
      play: false,
      pause: true,
      next: false,
      previous: false,
      currentStatus: ""
    }

    this.onPlay = this.onPlay.bind(this)
    this.onPause = this.onPause.bind(this)
    this.onNext = this.onNext.bind(this)
    this.onPrevious = this.onPrevious.bind(this)
  }

  onPlay () {
    fetch('http://127.0.0.1:8080/play', {
      method: 'POST',
      headers: {
        'Accept': 'multipart-form',
        'Content-Type': 'multipart-form'
      }
    }).then(this.setState({currentStatus: "Playing"}))
  }

  onPause () {
    fetch('http://127.0.0.1:8080/pause', {
      method: 'POST',
      headers: {
        'Accept': 'multipart-form',
        'Content-Type': 'multipart-form'
      }
    }).then(this.setState({currentStatus: "Paused"}))
  }

  onNext () {
    fetch('http://127.0.0.1:8080/next', {
      method: 'POST',
      headers: {
        'Accept': 'multipart-form',
        'Content-Type': 'multipart-form'
      }
    })
  }

  onPrevious () {
    fetch('http://127.0.0.1:8080/previous', {
      method: 'POST',
      headers: {
        'Accept': 'multipart-form',
        'Content-Type': 'multipart-form'
      }
    })
  }


  render () {
    return (
      <div className='row'>
      <div className='column'>
        <img onClick={this.onPrevious} defaultValue={this.state.previous}
          src={"/" + require('../forms/icons/previous_btn.png')}
          className='control-icons' alt='Previous' />
      </div>
        <div className='column'>
          <img onClick={this.onPlay} defaultValue={this.state.play}
            src={"/" + require('../forms/icons/play_btn.png')}
            className='control-icons' alt='Play' />
        </div>
        <div className='column'>
          <img onClick={this.onPause} defaultValue={this.state.pause}
            src={"/" + require('../forms/icons/pause_btn.png')}
            className='control-icons' alt='Pause' />
        </div>
        <div className='column'>
          <img onClick={this.onNext} defaultValue={this.state.next}
            src={"/" + require('../forms/icons/next_btn.png')}
            className='control-icons' alt='Next' />
        </div>
        <div className='column'>
          {this.state.currentStatus}
        </div>
      </div>
    )
  }
}

export default VideoControl
