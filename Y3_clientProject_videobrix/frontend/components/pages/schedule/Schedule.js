import React, { Component } from 'react'

import NumberDropDown from '../../forms/NumberDropDown'
import DayDropDown from '../../forms/DayDropDown'
import SettingsButton from '../../forms/SettingsButton'

import Header from '../layout/Header'
import Container from '../layout/Container'
import './Schedule.sass'

// Global reference for logic used for using states to submit form data.
// https://reactjs.org/docs/forms.html LAST ACCESSED ON 21/03/2019

//  REF:Props and components
// https://reactjs.org/docs/components-and-props.html LAST ACCESSED ON 21/03/2019

//  REF: States and lifecycle.
// https://reactjs.org/docs/state-and-lifecycle.html LAST ACCESSED ON 21/03/2019
class Schedule extends Component {
  constructor(props) {
    super(props)

    // Setting default values for states that will be used to submit
    // a requst to set a schedule.
    this.state = {
      day: 'Monday',
      hour: '00',
      minute: '00',
      video: 'null'
    }

    // Binding functions to this compnent to make them usable.
    // required because of Javascript limitations.
    this.onDayChange = this.onDayChange.bind(this)
    this.onHourChange = this.onHourChange.bind(this)
    this.onMinuteChange = this.onMinuteChange.bind(this)
    this.onRequestToSchedule = this.onRequestToSchedule.bind(this)
  }

  // When component mounts it requests the video ID from the URL so that
  // component knows which videos is required to be scheduled.
  componentWillMount() {
    // How to access URL parameters using react router v4
    // REFERENCE: https://jaketrent.com/post/access-route-params-react-router-v4/
    const { videoTitle } = this.props.match.params
    // end of referenced code.

    this.setState({ video: videoTitle })
  }

  // When day input changes state changes.
  onDayChange(userInput) {
    this.setState({ day: userInput })
  }

  // When hour input changes state changes.
  onHourChange(userInput) {
    this.setState({ hour: userInput })
  }

  // When minute input changes state changes.
  onMinuteChange(userInput) {
    this.setState({ minute: userInput })
  }

  // On click to button this function is called which sends a request
  // The request takes all the states stored and sends them to appropriate
  // server end point which schedules a video.
  // Reference on why .then methods are used
  // https://scotch.io/tutorials/how-to-use-the-javascript-fetch-api-to-get-data
  // LAST ACCESSED ON 21/03/2019
  onRequestToSchedule() {
    var time = String(this.state.hour) + ':' + String(this.state.minute)
    fetch('http://127.0.0.1:8080/api/set/schedule', {
      method: 'POST',
      headers: {
        'Accept': 'multipart-form',
        'Content-Type': 'multipart-form'
      },
      body: JSON.stringify({
        day: this.state.day,
        time: time,
        video: this.state.video
      })

    })
      .then(response => response.json())
      .then(data => console.log(data))
      .catch(err => {
        console.log(err)
      })
  }

  // Renders UI for scheduling videos inside a div
  // displays video ID to indicate user what video is being set.
  // Renders components to set states and use functions binded above.
  render() {
    return (
      <React.Fragment>
        <Header />
        <Container>
          <div className='video-scheduler-container'>
            <div className='video-title-display'>
              Scheduling: {this.state.video}
            </div>

            <NumberDropDown min={0} max={24} onChangeData={this.onHourChange} />
            <NumberDropDown min={0} max={60} onChangeData={this.onMinuteChange} /> <br />
            <DayDropDown onChangeData={this.onDayChange} /> <br />

            <SettingsButton name='Schedule' className='button-save' clickAction={this.onRequestToSchedule} />
          </div>
        </Container>
      </React.Fragment>

    )
  }
}

export default Schedule
