// Imports
import React from 'react'
import Card from '../../generic/Card'
import ReactDOM from 'react-dom'
import './videoPreview.sass'
import ThinCard from '../../generic/ThinCard'


// This page displays the videos that the user has selected on a modal
const main = document.getElementById('root')

export default class VideoPreview extends React.Component {
  constructor (props) {
    super(props)
    this.videoname = this.props.title
    this.state = {
      showPreview: true
    }
  }

  handleClick (e) {
    e.preventDefault()
    this.props.callback()
  }

  render () {
    return ReactDOM.createPortal(
      <div className='video-preview-modal'>
        <Card>
          <button className='x-button' onClick={this.props.closePopup}>
            <img src={'/' + require('../../forms/icons/close.png')} />
          </button>
          <video
            className='grid-video'
            id='video'
            preload='none'
            autoPlay='autoplay'
            controls
          >
          {/*Gets videos from jinja file on localhost (from flask server)*/}
          <source
             src={"http://localhost:8080/static/images/" + this.props.title}
            type='video/mp4'
          />

						Your browser does not support the video tag.
          </video>
        </Card>
      </div>,

      main
    )
  }
}
