// Imports
import React from 'react'
import Popup from '../../generic/Popup'
import VideoPreview from './VideoPreview'

// import VideoThumbnail from 'react-video-thumbnail' // use npm published version
import './videoLibrary.sass'

export default class Video extends React.Component {
  constructor(props) {
    super(props)
    this.videoname = this.props.video

    this.state = {
      showPopup: false,
      showPreview: false
    }
    this.video = React.createRef()
    this.handleDeleteImage = this.handleDeleteImage.bind(this)
  }

  // Dan: Pair programmed with Batu, seperating the video from the video page.
  onScheduleRequest() {
    // How to change window location. https://stackoverflow.com/questions/42914666/react-router-external-link 15/03/2019
    var urlLoc = '/schedule/' + this.props.video
    window.location = urlLoc
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    })
  }
  togglePreview() {
    this.setState({
      showPreview: !this.state.showPreview
    })
  }

  // Ajax request to delete the video off the Flask server and Sqlite database
  handleDeleteImage(ev) {
    let token = localStorage.getItem('jwt')
    fetch('http://127.0.0.1:8080/api/delete', {
      method: 'POST',
      body: JSON.stringify({ filename: this.props.video }),
      headers: {
        'Content-type': 'application/json',
        Authorization: 'Bearer ' + token
      }
    })
      .then(() => {
        console.log('Video deleted')
        this.props.delete()
        this.togglePopup()
      })
      .catch(err => {
        console.log(err)
      })
  }

  render() {
    return (
      // the ref points to this dif element.
      <div className='grid-item' ref={this.video}>
        <table>
          <tbody>
            <tr>
              <th className='video-name-container'>
                <h1>{this.props.name}</h1>
              </th>
              <th className='delete-icon-container'>
                <img
                  className='deleteIcon'
                  src={'/' + require('../../forms/icons/bin.png')}
                  onClick={this.togglePopup.bind(this)}
                />
              </th>
              <th className='delete-icon-container'>
                <img
                  className='deleteIcon'
                  src={'/' + require('../../forms/icons/time.png')}
                  onClick={this.onScheduleRequest.bind(this)}
                />
              </th>
            </tr>
          </tbody>
        </table>
        {this.state.showPopup ? (
          <Popup
            title='Delete this video?'
            // change name of popup inside.
            closePopup={this.togglePopup.bind(this)}
            // https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect
            // https://swizec.com/blog/ref-callbacks-measure-react-component-size/swizec/8444
            // used to find the possition and then used
            // to place the popup inside the grid item itstead of floating off to the left
            // gets the ref to the div - this.video.current is the ref and then run the function on the ref
            // thats called getBoundingClientRect that gets the location of the HTML location
            x={this.video.current.getBoundingClientRect().x}
            y={this.video.current.getBoundingClientRect().y}
            callback={this.handleDeleteImage}
          />
        ) : null}

{/*Gets video from Jinja on localhost using video name and displays it in grid*/}
        <p>{this.props.description}</p>
        <video className='grid-video' id='video'>
          <source
            src={"http://localhost:8080/static/images/" + this.props.video}
            type='video/mp4'
          />
          Your browser does not support the video tag.
        </video>
        {/*Display over video with play button and sheer opacity. Changes color on hover*/}
        <div className='play-overlay' onClick={this.togglePreview.bind(this)}>
          <img src={'/' + require('../../forms/icons/play-button.png')} />
        </div>
{/*Opens the modal to play video*/}
        {this.state.showPreview ? (
          <VideoPreview
            title={this.videoname}
            closePopup={this.togglePreview.bind(this)}
          />
        ) : null}
      </div>
    )
  }
}
