// Imports
import React from 'react'
import jwt from 'jsonwebtoken'
import Header from '../layout/Header'
import Container from '../layout/Container'
import Video from './Video'
import './videoLibrary.sass'
// This page will display all the videos in a grid, with a searchbar and upload button
export default class VideoLibrary extends React.Component {
  constructor (props) {
    super(props)
    this.FilterData = this.FilterData.bind(this)
    this.deleteVideo = this.deleteVideo.bind(this)
    this.onRedirect = this.onRedirect.bind(this)

    this.state = {
      imageURL: '',
      data: null,
      count: null,
      noVideos: false
    }
  }

  onRedirect () {
    this.props.history.push('/videos-upload')
  }

// Gets all videos and number of videos from flask
  componentDidMount () {
    let token = localStorage.getItem('senderboxjwt')
    if (!token) { this.setState({ errors: { message: 'Not connected to a senderbox' } }); return false }
    let jwtdata = jwt.decode(token)
    this.currentpi = jwtdata.ip
    fetch(`http://${this.currentpi}/api/allVideos`)
      .then(response => response.json())
      .then(data => {
        if (data[0] != null) {
          this.setState({ data })
        } else {
          this.setState({
            noVideos: !this.state.noVideos
          })
        }
      })
    fetch(`http://${this.currentpi}/api/allVideosCount`)
      .then(response => response.json())
      .then(count => {
        this.setState({ count })
      })
  }

  deleteVideo (id) {
    let data = this.state.data.filter((v, i) => i != id)
    // sorts through the array to delete video from the page
    // https://medium.com/@AndrewBonner2/filter-results-with-react-f746dc7984c
    this.setState({ data })
    let searchData = new FormData()
    searchData.append('searchItem', this.searchItem.value)
    fetch('http://127.0.0.1:8080/api/filter', {
      method: 'POST',
      body: searchData
    })
    // Data is returned depending on the criteria inputted in the search bar
      .then(response => response.json())
      .then(data => {
        if (data[0] != null) {
          console.log(data)
          this.setState({ data })
          fetch('http://127.0.0.1:8080/api/filterCount', {
            method: 'POST',
            body: searchData
          })
            .then(response => response.json())
            .then(count => {
              this.setState({ count })
            })
        } else {
          // If there is no data that matches the criteria, the all videos functions are used instead
          fetch('http://127.0.0.1:8080/api/allVideos')
            .then(response => response.json())
            .then(data => {
              if (data[0] != null) {
                this.setState({ data })
              } else {
                this.setState({
                  noVideos: !this.state.noVideos
                })
              }
            })
          fetch('http://127.0.0.1:8080/api/allVideosCount')
            .then(response => response.json())
            .then(count => {
              this.setState({ count })
            })
        }
      })
  }

  // When data is inputted into the search bar, this function will be triggered
  FilterData (ev) {
    ev.preventDefault()
    let searchData = new FormData()
    searchData.append('searchItem', this.searchItem.value)
    fetch(`http://${this.currentpi}/api/filter`, {
      method: 'POST',
      body: searchData
    })
    // Data is returned depending on the criteria inputted in the search bar
      .then(response => response.json())
      .then(data => {
        if (data[0] != null) {
          console.log(data)
          this.setState({ data })
          fetch(`http://${this.currentpi}/api/filterCount`, {
            method: 'POST',
            body: searchData
          })
            .then(response => response.json())
            .then(count => {
              this.setState({ count })
            })
        } else {
          fetch(`http://${this.currentpi}/api/allVideos`)
            .then(response => response.json())
            .then(data => {
              this.setState({ data })
            })
          fetch(`http://${this.currentpi}/api/allVideosCount`)
            .then(response => response.json())
            .then(count => {
              this.setState({ count })
            })
        }
      })
  }

  render () {
    return (
      <React.Fragment>
        <Header>
          {/* The top bar with the name of the page and the count of how many videos displayed */}
          {this.state.count
            ? this.state.count.map((item, i) => {
              return (
                <h1 key={i}>
                  <img
                    src={'/' + require('../../forms/icons/video.png')}
                    width='3%'
                    height='2.5%'
                    className='video_icon'
                  />
                  {item[0]}
                </h1>
              )
						  })
            : null}
        </Header>
        <Container>
          {/* Search with on input listener to trigger the filter function */}
          <div className='search-bar'>
            <form className='example' action='#'>
              <button className='search_button'>
                <img src={'/' + require('../../forms/icons/search.png')} width='12px' height='12px' />
              </button>
              <input
                type='text'
                placeholder=' Search..'
                name='search'
                onInput={this.FilterData}
                ref={ref => {
                  this.searchItem = ref
                }}
              />
            </form>
          </div>
          {/* All data returned from the SQL will be rendered into the grid below */}

          <div className='grid-container'>
          {/* The final grid item will be the upload video button */}
          <div className='grid-item'>
            <button className='upload_button' onClick={this.onRedirect}>
              <h3>Upload Videos</h3>
              <img src={'/' + require('../../forms/icons/upload.png')} width='20%' height='30%' className='uploadIcon' />
            </button>
          </div>
            {this.state.data
              ? this.state.data.map((item, i) => {
                return (
                  <Video
                    delete={() => {
                      this.deleteVideo(i)
                    }}
                    key={i}
                    name={item[0]}
                    video={item[1]}
                    description={item[2]}
                    thumbnail={item[3]}
                  />
                )
							  })
              : null}
            {this.state.noVideos ? (
              <div className='no-vids'>
                <h3>You have no videos</h3>
              </div>
            ) : null}
          </div>
        </Container>
      </React.Fragment>
    )
  }
}
