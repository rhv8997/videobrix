// Imports
import React from 'react'
import PropTypes from 'prop-types'

import './error.sass'

export default class ErrorContainer extends React.Component {
  render () {
    return (
      <div className='error-message'>
        <h1>{this.props.message}</h1>
      </div>
    )
  }
}

ErrorContainer.propTypes = {
  message: PropTypes.string
}

//
