// Imports
import React from 'react'
import { NavLink } from 'react-router-dom'

// React components
import Header from '../layout/Header'
import Container from '../layout/Container'

export default class ErrorNotFound extends React.Component {
  render () {
    return (
      <React.Fragment>
        <Header>
          <h1>Page not found</h1>
        </Header>
        <Container>
          <h3>Oops, this page does not exist, would you like to head to the <NavLink to='/dashboard'>dashboard</NavLink> ?</h3>
        </Container>
      </React.Fragment>)
  }
}

/// a
