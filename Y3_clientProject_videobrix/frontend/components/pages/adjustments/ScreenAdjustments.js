import React, { Component } from 'react'

import RangeSelection from '../../forms/RangeSelection'
import RotationButton from '../../forms/RotationButton'
import SettingsButton from '../../forms/SettingsButton'
import './ScreenAdjustments.sass'
import Header from '../layout/Header'
import Container from '../layout/Container'


// Global reference for logic used for using states to submit form data.
// https://reactjs.org/docs/forms.html LAST ACCESSED ON 21/03/2019

//  REF:Props and components
// https://reactjs.org/docs/components-and-props.html LAST ACCESSED ON 21/03/2019

//  REF: States and lifecycle.
// https://reactjs.org/docs/state-and-lifecycle.html LAST ACCESSED ON 21/03/2019
class ScreenAdjustments extends Component {
  constructor(props) {
    super(props);

    // Settings states here that will be altered and used to push values to server.
    this.state = {
      brightness: 50,
      contrast: 50,
      saturation: 50,
      red: 50,
      green: 50,
      blue: 50,
      rotation: 90
    };

    // Binding functions to this component so that they could be called from anywhere.
    // E.g. to be used as callback functions or to be used as anywhere in the code.
    this.onBrightnessChange = this.onBrightnessChange.bind(this);
    this.onContrastChange = this.onContrastChange.bind(this);
    this.onSaturationChange = this.onSaturationChange.bind(this);
    this.onRotationChange = this.onRotationChange.bind(this);

    this.onRedChange = this.onRedChange.bind(this);
    this.onGreenChange = this.onGreenChange.bind(this);
    this.onBlueChange = this.onBlueChange.bind(this);

    this.onClickRestore = this.onClickRestore.bind(this);
    this.onManualRotationChange = this.onManualRotationChange.bind(this);
    this.setAllData = this.setAllData.bind(this);
    this.onRequestToSave = this.onRequestToSave.bind(this);
  }

  // Method that is run when the value is changed on the brightness fader.
  // This is also called when restore is clicked to apply last saved settings.
  onBrightnessChange(userInput) {
    this.setState({ brightness: userInput });

    // POST Request: https://stackoverflow.com/questions/38510640/how-to-make-a-rest-post-call-from-reactjs-code
    // Jul 21 '16 at 17:47 by Malvolio
    fetch("http://127.0.0.1:8080/api/set/brightness", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        brightness_level: userInput
      })
    });
    // End of reference.
  }

  // Method that is run when the value is changed on the contrast fader.
  // This is also called when restore is clicked to apply last saved settings.
  onContrastChange(userInput) {
    this.setState({ contrast: userInput });

    // POST Request: https://stackoverflow.com/questions/38510640/how-to-make-a-rest-post-call-from-reactjs-code
    // Jul 21 '16 at 17:47 by Malvolio
    fetch("http://127.0.0.1:8080/api/set/contrast", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        contrast_level: userInput
      })
    });
    // End of reference.
  }

  // Method that is run when the value is changed on the saturation fader.
  // This is also called when restore is clicked to apply last saved settings.
  onSaturationChange(userInput) {
    this.setState({ saturation: userInput });

    // POST Request: https://stackoverflow.com/questions/38510640/how-to-make-a-rest-post-call-from-reactjs-code
    // Jul 21 '16 at 17:47 by Malvolio
    fetch("http://127.0.0.1:8080/api/set/saturation", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        saturation_level: userInput
      })
    });
    // End of reference.
  }

  // Method that is run when the value is changed on the red gamma fader.
  // This is also called when restore is clicked to apply last saved settings.
  onRedChange(userInput) {
    this.setState({ red: userInput });

    // POST Request: https://stackoverflow.com/questions/38510640/how-to-make-a-rest-post-call-from-reactjs-code
    // Jul 21 '16 at 17:47 by Malvolio
    fetch("http://127.0.0.1:8080/api/set/red", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        red_level: userInput
      })
    });
    // End of reference.
  }

  // Method that is run when the value is changed on the green gamma fader.
  // This is also called when restore is clicked to apply last saved settings.
  onGreenChange(userInput) {
    this.setState({ green: userInput });

    // POST Request: https://stackoverflow.com/questions/38510640/how-to-make-a-rest-post-call-from-reactjs-code
    // Jul 21 '16 at 17:47 by Malvolio
    fetch("http://127.0.0.1:8080/api/set/green", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        green_level: userInput
      })
    });
    // End of reference.
  }

  // Method that is run when the value is changed on the blue gamma fader.
  // This is also called when restore is clicked to apply last saved settings.
  onBlueChange(userInput) {
    this.setState({ blue: userInput });

    // POST Request: https://stackoverflow.com/questions/38510640/how-to-make-a-rest-post-call-from-reactjs-code
    // Jul 21 '16 at 17:47 by Malvolio
    fetch("http://127.0.0.1:8080/api/set/blue", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        blue_level: userInput
      })
    });
    // End of reference.
  }

  // Method that is run when the value is changed on the rotation.
  // This function is called when the button is clicked and does not
  // take any parameters. All the calculations made according to client's request.
  onRotationChange() {
    var currentValue = 0;

    // Checking what will happen if another 90 degrees is added to the rotation.
    // this is done to make sure that rotation never goes more that 360 degrees.
    // If the calculation estimates that value will become 360 then
    // it sets value to 0 as on a circle 360 means coming back to starting point.
    if (this.state.rotation < 270) {
      currentValue = this.state.rotation + 90;

      this.setState({ rotation: currentValue });
    } else {
      this.setState({ rotation: currentValue });
    }

    // POST Request: https://stackoverflow.com/questions/38510640/how-to-make-a-rest-post-call-from-reactjs-code
    // Jul 21 '16 at 17:47 by Malvolio
    fetch("http://127.0.0.1:8080/api/set/rotation", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        rotation: currentValue
      })
    });
    // End of reference.
  }

  // This function is used for restoring values as when the values are restored
  // they need to be entered into the application using a paramater.
  // previous one does not support parameters so this method is used when
  // changes with parameters is required to be made on the rotation.
  onManualRotationChange(userInput) {
    this.setState({ rotation: userInput });
    fetch("http://127.0.0.1:8080/api/set/rotation", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        rotation: userInput
      })
    });
  }


  // This function takes all the states and sends and ajax request to server
  // end point that accepts inputs for all the settings and stores them into the DB.
  onRequestToSave() {
    fetch("http://127.0.0.1:8080/api/save/settings", {
      method: "POST",
      headers: {
        Accept: "multipart-form",
        "Content-Type": "multipart-form"
      },
      body: JSON.stringify({
        brightness: this.state.brightness,
        contrast: this.state.contrast,
        saturation: this.state.saturation,
        red: this.state.red,
        green: this.state.green,
        blue: this.state.blue,
        rotation: this.state.rotation
      })
    });
  }

  // This method takes data as input which is an array and then
  // sets all the states to incoming data.
  setAllData(data) {
    console.log(data);

    this.onBrightnessChange(data[0][1]);
    this.onContrastChange(data[0][2]);
    this.onSaturationChange(data[0][3]);
    this.onRedChange(data[0][4]);
    this.onGreenChange(data[0][5]);
    this.onBlueChange(data[0][6]);
    this.onManualRotationChange(data[0][7]);
  }

  // This function sends a request to server to fetch last saved settings.
  // then settings are passed into setAllData function which sets states to
  // incoming data from the server.
  onClickRestore() {
    fetch("http://127.0.0.1:8080/api/lastSettings")
      .then(response => response.json())
      .then(data => this.setAllData(data));
  }


  // when the component mounts states are restored to last saved settings
  // This enables user to carry on working from where they left.
  componentWillMount() {
    this.onClickRestore()
  }

  // Renders a table that holds all the components binded to functions defined above.
  // Table is used to display data in proper alignment.
  render() {
    return (
      <React.Fragment>
        <Header />
        <Container>
          <div>
            <div className="buttons-container">
              <SettingsButton
                name="Save Settings"
                className="settings-button"
                clickAction={this.onRequestToSave}
              />
              <SettingsButton
                name="Restore To Last Saved"
                className="settings-button"
                clickAction={this.onClickRestore}
              />
            </div>
            <table className="controls-align-table">
              <tbody>
                <tr>
                  <td></td>
                  <td>General Adjustments</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    <img
                      src={require("../../forms/icons/brightness.png")}
                      className="control-icons"
                      alt="Brightness"
                    />
                  </td>
                  <td>
                    <RangeSelection
                      onChangeData={this.onBrightnessChange}
                      defaultValue={this.state.brightness}
                    />
                  </td>
                  <td>
                    <label className="current-data-display">
                      {this.state.brightness}
                    </label>
                  </td>
                </tr>

                <tr>
                  <td>
                    <img
                      src={require("../../forms/icons/contrast.png")}
                      className="control-icons"
                      alt="Contrast"
                    />
                  </td>
                  <td>
                    <RangeSelection
                      onChangeData={this.onContrastChange}
                      defaultValue={this.state.contrast}
                    />
                  </td>
                  <td>
                    <label className="current-data-display">{this.state.contrast}</label>
                  </td>
                </tr>

                <tr>
                  <td>
                    <img
                      src={require("../../forms/icons/rgb.png")}
                      className="control-icons"
                      alt="Saturation"
                    />
                  </td>
                  <td>
                    <RangeSelection
                      onChangeData={this.onSaturationChange}
                      defaultValue={this.state.saturation}
                    />
                  </td>
                  <td>
                    <label className="current-data-display">
                      {this.state.saturation}
                    </label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <RotationButton onRotationRequest={this.onRotationChange} />
                  </td>
                  <td />
                  <td>
                    <label className="current-data-display">{this.state.rotation}</label>
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>Gamma Correction</td>
                  <td></td>
                </tr>
                <tr>
                  <td>Red</td>
                  <td>
                    <RangeSelection
                      onChangeData={this.onRedChange}
                      defaultValue={this.state.red}
                      max={255}
                    />
                  </td>
                  <td>
                    <label className="current-data-display">{(this.state.red / 25.5).toFixed(1)}</label>
                  </td>
                </tr>

                <tr>
                  <td>Green</td>
                  <td>
                    <RangeSelection
                      onChangeData={this.onGreenChange}
                      defaultValue={this.state.green}
                      max={255}
                    />
                  </td>
                  <td>
                    <label className="current-data-display">{(this.state.green / 25.5).toFixed(1)}</label>
                  </td>
                </tr>

                <tr>
                  <td>Blue</td>
                  <td>
                    <RangeSelection
                      onChangeData={this.onBlueChange}
                      defaultValue={this.state.blue}
                      max={255}
                    />
                  </td>
                  <td>
                    <label className="current-data-display">{(this.state.blue / 25.5).toFixed(1)}</label>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

export default ScreenAdjustments
