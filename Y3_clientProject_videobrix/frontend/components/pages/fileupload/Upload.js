// Imports
import React from 'react'

// React Components
import Header from '../layout/Header'
import Container from '../layout/Container'
import Card from '../../generic/card'
import history from '../../../src/history'

import '../../forms/form.sass'
import './fileupload.sass'

const allowed = ['jpg', 'jpeg', 'mp4', 'png', 'gif']
const imageExtensions = ['jpg', 'jpeg', 'png', 'gif']
const loadVideo = (src) => {
  //Loadvideo function parameters source (type https://developer.mozilla.org/en-US/docs/Web/API/File)
  return new Promise((resolve, reject) => {
    //create a promise so the function is asynchronous
    let video = document.createElement('video');
    //create the video element in the document
    video.preload = 'metadata';

    video.onloadedmetadata = function () {
      //When the metadata about the video has loaded (duration author etc)
      window.URL.revokeObjectURL(video.src);
      //Delete the created media stream on line 30
      resolve(video.duration)
      //resolve (sending back the video duration)
    }

    video.src = URL.createObjectURL(src)
    //set the source for the video by creating a blob from a file object
  })
}

const createForm = async (ctx) => {
  const format = ctx.uploadInput.files.length !== 0 ? ctx.uploadInput.files[0].type.split("/")[1] : null
  const isImage = imageExtensions.includes(format)
  let data = new FormData()
  // let so that you can change the data
  data.append('file', ctx.uploadInput.files[0])
  data.append('name', ctx.fileName.value)
  data.append('description', ctx.fileDescription.value)
  if (isImage) { data.append('imagetimer', ctx.imageTime.value) } else {
    let duration = await loadVideo(ctx.uploadInput.files[0])
    // await forces asynchronous functions to wait for the code to bedone before continuing
    data.append('imagetimer', Math.floor(duration))
  }
  return data
}

export default class Upload extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      imageURL: '',
      file: null,
      errors: [],
      isImage: false,
    }

    this.handleUploadImage = this.handleUploadImage.bind(this)
    this.imageChosen = this.imageChosen.bind(this)
  }
  imageChosen() {
    const format = this.uploadInput.files[0].type.split("/")[1]
    // splits it into an array by  slash and then get 1st element out of the array
    this.setState({ file: this.uploadInput.files[0].name, isImage: imageExtensions.includes(format) })
    // if array contains image file it returns true.
  }

  async handleUploadImage(ev) {
    ev.preventDefault()
    let errors = []
    const format = this.uploadInput.files.length !== 0 ? this.uploadInput.files[0].type.split("/")[1] : null
    const isImage = imageExtensions.includes(format)
    // check if the file format is allowed
    console.log(isImage)
    if (!isImage && imageExtensions.includes(format)) {
      errors.push({ error: `File format ${format} is not allowed!` })
    }
    if (isImage && this.imageTime.value === '') {
      errors.push({ error: 'The Image time is empty' })
    }
    if (this.fileName.value === '') {
      // if field is empty, log to console and put "file name empty in the new error array"
      console.log('The File Name is empty')
      errors.push({ error: 'The File name is empty' })
      // push this error msg into the array
    }
    if (!this.legalTick.checked) {
      // if field is empty, log to console and put "file name empty in the new error array"
      console.log('You can not submit a file without accepting the disclaimer')
      errors.push({ error: 'You can not submit a file without accepting the disclaimer' })
      // push this error msg into the array
    }
    if (this.fileDescription.value === '') {
      console.log('The File Description is empty')
      // if field is empty, log to console and put "file descripton empty in the new error array"
      errors.push({ error: 'The File Description is empty' })
    }

    // if the array is not empty, so there is errors, kill the submit.
    if (errors.length !== 0) {
      this.setState({ errors })
      return false
    }

    // Code Reference: Code idea from a guide on medium.com
    //https://medium.com/@ashishpandey_1612/file-upload-with-react-flask-e115e6f2bf99
    const data = await createForm(this)
    let token = localStorage.getItem('jwt')
    console.log(data)
    // AJAX ruest to send the data to Flask server
    fetch('http://127.0.0.1:8080/api/upload', {
      method: 'POST',
      body: data,
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(() => {
        // navigate away from page once finished.
        console.log('Video Uploaded')
        history.push('/videos')
      })
      .catch(err => {
        console.log(err)
      })
  }
  render() {
    console.log(this.state.errors)
    return (
      <React.Fragment>
        <Header>
          <h1>Upload video</h1>
        </Header>
        <Container>
          <Card>
            <div className='upload-form'>
              <form onSubmit={this.handleUploadImage} className='video-upload'>

                <div className='input-wrapper'>
                  <div className='input'>
                    <h1>File</h1>
                    <input
                      className='fileSelect'
                      alt='File Selected'
                      ref={ref => {
                        this.uploadInput = ref
                      }}
                      onChange={this.imageChosen}
                      type='file'
                      id='file'
                    />
                    <label htmlFor='file'>{this.state.file || 'Choose a video'}</label>
                  </div>
                </div>
                <br />
                <div className='input-wrapper'>
                  <div className='input'>
                    <h1>File name</h1>
                    <input
                      className='fileName'
                      alt='File Name'
                      ref={ref => {
                        this.fileName = ref
                      }}
                      type='text'
                      placeholder='File Name'
                    />
                  </div>
                </div>
                <div className='input-wrapper'>
                  <div className='input'>
                    <h1>Description:</h1>
                    <input
                      className='fileDescription'
                      alt='File Description'
                      ref={ref => {
                        this.fileDescription = ref
                      }}
                      type='text'
                      placeholder='Description'
                    />
                  </div>
                </div>
                {this.state.isImage ?
                  <div className='input-wrapper'>
                    <div className='input'>
                      <h1>Time</h1>
                      <input
                        className='time'
                        alt='time'
                        ref={ref => {
                          this.imageTime = ref
                        }}
                        type='number'
                        placeholder='Length of time to display the image in seconds'
                      />
                    </div>
                  </div> : null}
                <div className='input-wrapper '>
                  <div className='input checkbox'>
                    <input type='checkbox' className='legalTick' value='tickSelected' alt='legalTick'
                      ref={ref => {
                        this.legalTick = ref
                      }} />
                    <h1>By clicking upload you confirm that you have the rights to use the video you uploaded commercially.</h1>
                    <br />
                  </div>
                </div>
                <div className='input-wrapper'>
                  <div className='input button'>
                    <input type='submit' value='upload' />
                  </div>
                </div>
                {this.state.errors.length !== 0 ? (
                  // map loops through the array of errors and returns individual
                  <div className='validationWarning'>
                    {this.state.errors.map(error => {
                      // prints the errro array with the error messages are inside.
                      return <h1>Error: {error.error}</h1>
                    })}
                  </div>
                ) : null}
              </form>
            </div>
          </Card>
        </Container>
      </React.Fragment>
    )
  }
}
