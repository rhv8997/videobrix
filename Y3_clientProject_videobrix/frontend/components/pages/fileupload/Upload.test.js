import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai'
import Upload from './Upload';
import Container from '../layout/Container'
// Reference:
//https://stackoverflow.com/questions/48094581/testing-react-portals-with-enzyme
describe('Modal component', () => {

    const component = shallow(<Upload />)
    it('should render the styled components and the children', () => {
        console.log(component.find('.button'))
        // check if button is even there
        expect(component.find(Container).find('.button').shallow().find('input[type="submit"]')).to.have.lengthOf(1)
        // check to see if the validation is not there
        expect(component.find(Container).find('.validationWarning')).to.have.lengthOf(0)
        // test to see if the  legal check button is there
        expect(component.find(Container).find('.legalTick').shallow().find('input[type="checkbox"]')).to.have.lengthOf(1)
        // test to see if the input file is present
        expect(component.find(Container).find('.fileSelect').shallow().find('input[type="file"]')).to.have.lengthOf(1)
        //check that file name has loaded correctly 
        expect(component.find(Container).find('.fileName').shallow().find('input[type="text"]')).to.have.lengthOf(1)
        // check that file description input has loaded correctly
        expect(component.find(Container).find('.fileDescription').shallow().find('input[type="text"]')).to.have.lengthOf(1)
        // check that the timer doesnt load up unless image is selected.
        expect(component.find(Container).find('.input').find('input[type="number"]')).to.have.lengthOf(0)
        expect(component.find(Container).find('.input button').find('input[type="submit"]')).to.have.lengthOf(0)


    });

});