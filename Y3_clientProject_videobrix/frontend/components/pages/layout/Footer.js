// Imports
import React from "react";
// Style
import "./layout.sass";

export default class Footer extends React.Component {

	render() {
		return (
			<div className="footer"> <footer>Icons made by Freepik, Smashicons, EpicCoders, Icon Works Gregor Cresnar and Cole Bemis under Creative Commons BY 3.0 from www.flaticon.com </footer></div>
		);
	}
}
