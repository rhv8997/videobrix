import React from 'react'
import PropTypes from 'prop-types'
import Footer from "./Footer"

export default class Container extends React.Component {
  render () {
    return <div className='main-container'>{this.props.children} <Footer /></div>
  }
}

Container.propTypes = {
  children: PropTypes.node
}
