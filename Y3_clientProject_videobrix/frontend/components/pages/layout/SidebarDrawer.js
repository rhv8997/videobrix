// Imports
import React from "react";
import { HashRouter as Router, Route, NavLink, Switch } from "react-router-dom";
import "./sidebar.sass";

// This sidebar appears when screensize is reduced and menu button is pressed
export default class SidebarDrawer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visibility: true
		};
		this.toggleVisibility = this.toggleVisibility.bind(this);
	}
	// This toggles if the sidebar drawer will be displayed or not
	toggleVisibility() {
		this.setState({
			visibility: !this.state.visibility
		});
	}
	// This is the new sidebar drawer (same content and functionality as the other sidebar)
	render() {
		if (this.state.visibility) {
			return (
				<div className="sidebarDrawer">
					<div className="SidebarDrawer-expand">
						<div className="SidebarDrawer-header" onClick={this.toggleVisibility}>
							<h1>
								<img
									src={"/" + require("../../forms/icons/menu.png")}
									className="burger-icon"
								/>
								&nbsp; VIDEOBRIX
							</h1>
						</div>

						<ul id="navbar">
							<NavLink activeClassName="active-link" to="/videos">
								<li className="MenuItem active">
									<div className="list-item-content">
										<img src={"/" + require("../../forms/icons/video-camera.png")} />
										Videos
									</div>
								</li>
							</NavLink>
							<NavLink activeClassName="active-link" to="/playlists">
								<li className="MenuItem">
									<div className="list-item-content">
										<img src={"/" + require("../../forms/icons/playlist.png")} />
										Playlists
									</div>
								</li>
							</NavLink>
							<NavLink activeClassName="active-link" to="/senderboxes">
								<li className="MenuItem">
									<div className="list-item-content">
										<img src={"/" + require("../../forms/icons/television.png")} />
										Screens
									</div>
								</li>
							</NavLink>

							<NavLink activeClassName="active-link" to="/adjustments">
								<li className="MenuItem">
									<div className="list-item-content">
										<img src={"/" + require("../../forms/icons/controls.png")} />
										Screen Controls
									</div>
								</li>
							</NavLink>
							<NavLink activeClassName="active-link" to="/manageSchedules">
								<li className="MenuItem">
									<div className="list-item-content">
										<img src={'/' + require('../../forms/icons/time.png')} />
										Schedules
									</div>
								</li>
							</NavLink>
							<NavLink activeClassName="active-link" to="/videos-upload">
								<li className="MenuItem">
									<div className="list-item-content">
										<img src={'/' + require('../../forms/icons/upload.png')} />
										Upload Video
								</div>
								</li>
							</NavLink>
							<NavLink activeClassName="active-link" to="/support">
								<li className="MenuItem">
									<div className="list-item-content">
										<img src={"/" + require("../../forms/icons/headset.png")} />
										Support
									</div>
								</li>
							</NavLink>

						</ul>
					</div>
				</div>
			);
		} else {
			return <div />;
		}
	}
}
