// Imports
import React from "react";
import PropTypes from "prop-types";
import axios from "axios";

import Sidebar from "./Sidebar";
import SidebarDrawer from "./SidebarDrawer";

// React components
import SenderboxLocations from "./SenderboxLocations";

// Style
import "./layout.sass";

export default class TopBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: null,
			visibility: false
		};
		this.toggleVisibility = this.toggleVisibility.bind(this);
		this.onLogoutClick = this.onLogoutClick.bind(this);
	}
	toggleVisibility() {
		this.setState({
			visibility: !this.state.visibility
		});
	}
	componentDidMount() {
		let token = localStorage.getItem("jwt");
		axios
			.get(`/api/userinfo`, { headers: { Authorization: "Bearer " + token } })
			.then(resp => {
				console.log(resp.data.user);
				if (resp.data.status) {
					this.setState({ user: resp.data.user });
				} else {
					// else
				}
			});
	}

	onLogoutClick() {
		localStorage.removeItem("jwt");
		window.location = "/login"
	}

	render() {
		const { user, visibility } = this.state;
		console.log(user);
		return (
			<React.Fragment>
				{visibility ? <SidebarDrawer /> : null}
				<Sidebar />
				<div className="topbar">
					<img
						src={"/" + require("../../forms/icons/menu.png")}
						width="0"
						height="0"
						class="burger-menu"
						onClick={this.toggleVisibility}
					/>
					<h1>Videobrix</h1>
					{user ? (
						<div className="user-info">
							<div>
								<h1>{user.displayname}</h1>
								<SenderboxLocations
									locations={user.senderboxes}
									changeLocation={this.props.changeLocation}
								/>
							</div>
						</div>
					) : null}
					<button className= "settings-button right-margin" onClick={() => { this.onLogoutClick() }} >Logout</button>
				</div>
			</React.Fragment>
		);
	}
}

TopBar.propTypes = {
	changeLocation: PropTypes.func
};
//a
