// Imports
import React from 'react'

import PropTypes from 'prop-types'
import VideoControl from "../VideoControl"

// Style
import './layout.sass'
export default class Header extends React.Component {
  render () {
    return (
      <div className='pagebar'>
        <VideoControl />
        {this.props.children}
      </div>
    )
  }
}

Header.propTypes = {
  children: PropTypes.node
}
