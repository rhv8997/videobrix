// Imports
import React from 'react'
import PropTypes from 'prop-types'
import jwt from 'jsonwebtoken'
import axios from 'axios'

import history from '../../../src/history'

// Style
import './layout.sass'

export default class SenderboxLocations extends React.Component {
  constructor (props) {
    super(props)
    let val
    let jwtstorage = localStorage.getItem('senderboxjwt')
    if (!jwtstorage) {
      val = null
    } else {
      let cur = jwt.decode(jwtstorage)
      val = cur.id
    }
    this.state = { value: val }
    this.selectLocation = this.selectLocation.bind(this)
  }
  selectLocation (e) {
    let val = e.target.value
    if(val === "new"){
      history.push("/senderboxes/add")
      return false
    }
    this.setState({ value: val })
    this.connectToSenderbox(e.target.value).then(() => {
      this.props.changeLocation(val)
    })
  }
  connectToSenderbox (id) {
    let token = localStorage.getItem('jwt')
    if (!token) { return false }
    return axios.get(`/api/connect/${id}`, { headers: { Authorization: 'Bearer ' + token } }).then(resp => {
      if (resp.data.status) {
        localStorage.setItem('senderboxjwt', resp.data.token)
      }
    })
  }
  render () {
    const { value } = this.state
    let locations = this.props.locations || []
    console.log(locations)
    return (
      <select onChange={this.selectLocation} value={value || 'default'}>
        {!value ? <option value='default'>Pick a senderbox</option> : null}
        {locations.map((location, i) => {
          console.log(location)
          return (<option value={location.id} key={i} >{location.name}</option>)
        })}
        <option value="new" onClick={() => {}}>Select to add a senderbox</option>
      </select>
    )
  }
}

SenderboxLocations.propTypes = {
  locations: PropTypes.array,
  changeLocation: PropTypes.func
}

// a
