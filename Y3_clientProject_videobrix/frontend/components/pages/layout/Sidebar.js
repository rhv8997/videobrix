// Imports
import React from "react";
import { HashRouter as Router, Route, NavLink, Switch } from "react-router-dom";
import "./sidebar.sass";

// Displays list of all pages on site that can be navigated to by nav navbar
// Works with react router in app.js
export default class Sidebar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visibility: true
		};
		this.toggleVisibility = this.toggleVisibility.bind(this);
	}

// Hides sidebar on reduced screen size
	toggleVisibility() {
		this.setState({
			visibility: !this.state.visibility
		});
	}

	render() {
		// if (this.state.visibility) {
		return (
			<div className="sidebar">
				<div className="sidebar-expand">
					<div className="sidebar-header">
						<h1 onClick={this.toggleVisibility}>
						{/*Displays when screen size is reduced*/}
							<img
								src={"/" + require("../../forms/icons/menu.png")}
								width="10%"
								height="60%"
							/>
							MENU
						</h1>
					</div>

					<ul id="navbar">
						<NavLink activeClassName="active-link" to="/videos">
							<li className="MenuItem active">
								<div className="list-item-content">
									<img src={"/" + require("../../forms/icons/video-camera.png")} />
									Videos
								</div>
							</li>
						</NavLink>
						<NavLink activeClassName="active-link" to="/playlists">
							<li className="MenuItem">
								<div className="list-item-content">
									<img src={"/" + require("../../forms/icons/playlist.png")} />
									Playlists
								</div>
							</li>
						</NavLink>
						<NavLink activeClassName="active-link" to="/senderboxes">
							<li className="MenuItem">
								<div className="list-item-content">
									<img src={"/" + require("../../forms/icons/television.png")} />
									Screens
								</div>
							</li>
						</NavLink>

						<NavLink activeClassName="active-link" to="/adjustments">
							<li className="MenuItem">
								<div className="list-item-content">
									<img src={"/" + require("../../forms/icons/controls.png")} />
									Screen Controls
								</div>
							</li>
						</NavLink>
						<NavLink activeClassName="active-link" to="/manageSchedules">
							<li className="MenuItem">
								<div className="list-item-content">
									<img src={'/' + require('../../forms/icons/time.png')} />
									Schedules
								</div>
							</li>
						</NavLink>
						<NavLink activeClassName="active-link" to="/videos-upload">
							<li className="MenuItem">
								<div className="list-item-content">
									<img src={'/' + require('../../forms/icons/upload.png')} />
									Upload Video
								</div>
							</li>
						</NavLink>
						<NavLink activeClassName="active-link" to="/support">
							<li className="MenuItem">
								<div className="list-item-content">
									<img src={"/" + require("../../forms/icons/headset.png")} />
									Support
								</div>
							</li>
						</NavLink>


					</ul>
				</div>
			</div>
		);

	}
}
