// Imports
import React from 'react'
import axios from 'axios'

// React Components
import Header from '../layout/Header'
import Container from '../layout/Container'
import Form from '../../forms/Form'
import Textbox from '../../forms/TextBox'
import Submit from '../../forms/Submit'
import './manageAccount.sass'
import Card from '../../generic/card'
import history from '../../../src/history'

export default class AccountManagement extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      authenticating: false,
      auth: null,
      errors: []

    }
    this._handleFormSubmission = this._handleFormSubmission.bind(this)
  }

  _handleFormSubmission(formData) {
    this.setState({ authenticating: true })
    let token = localStorage.getItem('jwt')
    let errors = [] //creating empty array.

    console.log(formData.newPassword, formData.confirmPassword)


    // if the new password does not match the confirmed password, then cancel the submit
    // and then push the error passwords do not match into errors.
    // errors will then print the errors in the h1 at under the form to alert the user
    if (formData.newPassword !== formData.confirmPassword) {
      console.log('Passwords do not match')
      errors.push({ error: 'Passwords do not match' })

      // if the array is not empty, so there is errors, kill the submit.
      if (errors.length !== 0) {
        this.setState({ errors })
        return false
      }
    }
    else {
      // Refernce for using Axios.https://alligator.io/react/axios-react/
      // Also reference batu's login component request to use it and re using his container, text box
      // form and commit components.
      //axios raps the request in a different so that you can do more
      // This request is posting to the express server to interact with the mongo DB.
      axios
        .post(
          '/api/changePassword',
          {
            username: formData.username,
            password: formData.password,
            newPassword: formData.newPassword
          },
          {
            headers: { Authorization: 'Bearer ' + token }
          }
        )
        .then(response => {
          if (response.data.auth) {
            this.setState({ auth: true }, () => {
              setTimeout(() => {
                localStorage.setItem('jwt', response.data.token)
                // history.push('/dashboard')
              })
            })
          } else {
            this.setState({ authenticating: false })
          }
        })
    }
  }
  render() {
    return (
      <React.Fragment>
        <Header />
        <Container>
          <div className='changePass-wrapper'>
            <h2>Change Password:</h2>
            <div className='changePass-container-card'>
              <React.Fragment>
                <Card>
                  <Form method='POST' onSubmit={this._handleFormSubmission}>
                    <Textbox title='Username' name='username' required />
                    <Textbox
                      title='Old Password:'
                      type='password'
                      name='password'
                      ref={ref => {
                        this.oldPasswordref = ref
                      }}
                      pattern='(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}'
                      required
                    />
                    <Textbox
                      title='New Password:'
                      type='password'
                      name='newPassword'
                      ref={ref => {
                        this.newPasswordref = ref
                      }}
                      pattern='(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}'
                      required
                    />
                    <Textbox
                      title='Repeat New Password:'
                      type='password'
                      name='confirmPassword'
                      ref={ref => {
                        this.confirmPasswordref = ref
                      }}
                      pattern='(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}'
                      required
                    />
                    <Submit
                      value='Change   Password'
                      onFormSubmit={this._handleFormSubmission}
                    />
                  </Form>
                  {this.state.errors.length !== 0 ? (
                    // map loops through the array of errors and returns individual
                    <div className='validationWarning'>
                      {this.state.errors.map(error => {
                        // prints the errro array with the error messages are inside.
                        return <div className='errorWarning'>Error: {error.error}</div>
                      })}
                    </div>
                  ) : null}
                </Card>
              </React.Fragment>
            </div>
          </div>
        </Container>
      </React.Fragment>
    )
  }
}
