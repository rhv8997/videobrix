import React, { Component } from 'react'

import NumberDropDown from '../../forms/NumberDropDown'
import DayDropDown from '../../forms/DayDropDown'
import SettingsButton from '../../forms/SettingsButton'

import Header from '../layout/Header'
import Container from '../layout/Container'
import './ManageSchedules.sass'

// Global reference for logic used for using states to submit form data.
// https://reactjs.org/docs/forms.html LAST ACCESSED ON 21/03/2019

//  REF:Props and components
// https://reactjs.org/docs/components-and-props.html LAST ACCESSED ON 21/03/2019

//  REF: States and lifecycle.
// https://reactjs.org/docs/state-and-lifecycle.html LAST ACCESSED ON 21/03/2019
class ManageSchedules extends Component {
  constructor (props) {
    super(props)

    // Setting default value for state that is a list that is used
    // to list all the available schedules.
    this.state = {
      list: ['empty']
    }

    // Binding functions to component to satisfy requirement of Javascript.
    this.onDeleteRequest = this.onDeleteRequest.bind(this)
    this.getAllSchedules = this.getAllSchedules.bind(this)
  }

  // Fetches all the schedules from server which servers database data.
  // then the received value is set to be the state so that page displays
  // data from the DB.
  getAllSchedules () {
    fetch('http://127.0.0.1:8080/api/schedule/all')
      .then(response => response.json())
      .then(data => this.setState({ list: data }))
  }

  // Runs the fetching method when component mounts so that state change
  // is applied once the page is loaded.
  componentWillMount () {
    this.getAllSchedules()
  }

  // Function used to send a delete request server end point which handles
  // delete requests. ID of schedule is sent to server
  // so that server can run SQL command to find the record and delete it from the DB.
  // Once the delete action occurs list is fetched again to make sure
  // List displayed to user is up to date.
  // REFERENCE: When .then is used in the fetch logic.
  // https://scotch.io/tutorials/how-to-use-the-javascript-fetch-api-to-get-data
  // LAST ACCESSED ON 21/03/2019
  onDeleteRequest (videoID) {
    fetch('http://127.0.0.1:8080/api/delete/schedule', {
      method: 'POST',
      headers: {
        'Accept': 'multipart-form',
        'Content-Type': 'multipart-form'
      },
      body: JSON.stringify({
        video_id: videoID
      })

    })
      .then(response => this.getAllSchedules())
      .catch(err => {
        console.log(err)
      })
  }
  // Function inside the onclick error fix
  // REF: https://stackoverflow.com/questions/32937365/button-onclick-triggered-when-init-in-react-application 14/03/2019

  // Why list.map is used to create a loop instead of a regular for loop to create list of schedules.
  // https://reactjs.org/docs/lists-and-keys.html
  render () {
    return (
      <React.Fragment>
        <Header />
        <Container>
          <div className='schedules-container'>
            <table className='schedules-align-table'>
              <tbody>
                <tr key={'titles'}>
                  <td key={'day'} className='title'>DAY</td>
                  <td key={'time'} className='title'>TIME</td>
                  <td key={'video'} className='title'>VIDEO</td>
                  <td key={'action'} className='title'>ACTION</td>
                </tr>
                {
                  this.state.list.map((info) => {
                    return (
                      <tr key={info[0]}>
                        <td key={info[1]}>{info[1]}</td>
                        <td key={info[2]}>{info[2]}</td>
                        <td key={info[3]}>{info[3]}</td>
                        <td key={info[4]}>
                          <img src={require('../../forms/icons/trash-icon.png')} onClick={() => { this.onDeleteRequest(info[0]) }} className='delete-button' />
                        </td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>

          </div>
        </Container>
      </React.Fragment>

    )
  }
}

export default ManageSchedules
