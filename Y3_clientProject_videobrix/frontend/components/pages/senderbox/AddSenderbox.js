// Imports
import React from 'react'
import axios from 'axios'
// import PropTypes from 'prop-types'

// React Components
import Header from '../layout/Header'
import Container from '../layout/Container'
import Form from '../../forms/Form'
import TextBox from '../../forms/TextBox'
import Select from '../../forms/Select'
import Card from '../../generic/Card'

// Style
import './pis.sass'
import Submit from '../../forms/Submit'

export default class AddPi extends React.Component {
  constructor (props) {
    super(props)
    this.state = { keys: [], options: null, validIP: null, validSenderbox: null }
    this.pingPi = this.pingPi.bind(this)
    this.addSenderBox = this.addSenderBox.bind(this)
  }
  componentWillMount () {
    let token = localStorage.getItem('jwt')
    if (!token) { return false }
    axios.get(`/api/keys`, { headers: { Authorization: 'Bearer ' + token } }).then(resp => {
      if (resp.data.status) {
        let keys = []
        if (!resp.data.keys) {
          keys.splice(0, 0, { actualValue: 'newkeygen', shownValue: 'Create new key' })
        } else {
          keys = resp.data.keys.map(key => { return ({ actualValue: key._id, shownValue: key.name }) })
        }
        this.setState({ options: keys }, () => console.log(this.state))
      } else {
        this.setState({ error: resp.data.message })
      }
    })
  }
  pingPi (input) {
    let isValidIp = new RegExp(`^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$`).test(input)
    // regex https://www.oreilly.com/library/view/regular-expressions-cookbook/9780596802837/ch07s16.html
    if (isValidIp) {
      axios.get('http://' + input + ':8080/api/check').then(resp => {
        if (resp.status === 200) {
          this.setState({ validSenderbox: true, validIP: isValidIp })
        } else {
          this.setState({ validSenderbox: false, validIP: isValidIp })
        }
      }).catch(err => {
        console.log(err)
        this.setState({ validSenderbox: false, validIP: isValidIp })
      })
    } else {
      this.setState({ validSenderbox: false, validIP: isValidIp })
    }
  } // if its mid request and a proper ip is entered later on, has possibility of returning errors as it will say its no longer a valid ip due to request time
  addSenderBox (inputs) {
    if (this.state.validIP && this.state.validSenderbox) {
      console.log(inputs)
      let token = localStorage.getItem('jwt')
      axios.post('/api/addSenderbox', { ...inputs }, { headers: { Authorization: 'Bearer ' + token } }).then(resp => {
        console.log(resp)
      }).catch(err => {
        console.log(err)
      })
    }
  }
  render () {
    return (
      <React.Fragment>
        <Header>
          <h1>Add Senderbox</h1>
        </Header>
        <Container>
          {this.state.options
            ? <Card><Form onSubmit={this.addSenderBox}>
              <TextBox title='Senderbox Name' name='name' required />
              <TextBox title='Senderbox IP' name='ip' required after={500} do={this.pingPi}
                customErrors={[
                  { rule: 'IPisNotSenderbox', passed: this.state.validSenderbox, text: 'IP does not point to a Videobrix Senderbox' },
                  { rule: 'InvalidIP', passed: this.state.validIP, text: 'IP is not valid' }
                ]} />
              <Select title='Key to use' options={this.state.options} name='key' required />
              <Submit value='Add Senderbox' />
            </Form>
            </Card> : null }
        </Container>
      </React.Fragment>
    )
  }
}

// AddPi.propTypes = {

// }

// `new RegExp('[^@]+@[^@]+\.[^@]+')`
// new RegExp('[^@]+@[^@]+\\.[^@]+')
