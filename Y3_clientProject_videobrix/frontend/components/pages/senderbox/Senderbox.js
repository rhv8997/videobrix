// Imports
import React from 'react'
import PropTypes from 'prop-types'

// React Components
import ThinCard from '../../generic/ThinCard'

// Style
import './pis.sass'
import axios from 'axios'

export default class Senderbox extends React.Component {
  constructor (props) {
    super(props)
    this.state = { senderboxes: null }
    this.connectToSenderbox = this.connectToSenderbox.bind(this)
  }
  connectToSenderbox () {
    let token = localStorage.getItem('jwt')
    if (!token) { return false }
    axios.get(`/api/connect/${this.props.id}`, { headers: { Authorization: 'Bearer ' + token } }).then(resp => {
      if (resp.data.status) {
        localStorage.setItem('senderboxjwt', resp.data.token)
      }
    })
  }
  render () {
    return (
      <ThinCard>
        <div className='padding'>
          <h1>{this.props.name}</h1>
          <button onClick={this.connectToSenderbox}>Connect</button>
        </div>
      </ThinCard>
    )
  }
}

Senderbox.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string
}

// a
