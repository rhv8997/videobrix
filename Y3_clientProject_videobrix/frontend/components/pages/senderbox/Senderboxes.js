// Imports
import React from 'react'
import axios from 'axios'
// import PropTypes from 'prop-types'

// React Components
import Header from '../layout/Header'
import Container from '../layout/Container'
import Card from '../../generic/Card'

// Style
import './pis.sass'

export default class AddPi extends React.Component {
  constructor (props) {
    super(props)
    this.state = { senderboxes: null }
    this.pingPi = this.pingPi.bind(this)
  }
  componentWillMount () {
    let token = localStorage.getItem('jwt')
    if (!token) {
      return false
    }
    axios
      .get(`/api/senderboxes`, { headers: { Authorization: 'Bearer ' + token } })
      .then(resp => {
        if (resp.data.status) {
          this.setState({ senderboxes: resp.data.senderboxes })
        } else {
          this.setState({ error: resp.data.message })
        }
      })
  }
  pingPi (input) {
    let isValidIp = new RegExp(
      `^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$`
    ).test(input)
    // regex https://www.oreilly.com/library/view/regular-expressions-cookbook/9780596802837/ch07s16.html
    if (isValidIp) {
      axios
        .get('http://' + input + ':8080/api/check')
        .then(resp => {
          if (resp.status === 200) {
            this.setState({ validSenderbox: true, validIP: isValidIp })
          } else {
            this.setState({ validSenderbox: false, validIP: isValidIp })
          }
        })
        .catch(err => {
          console.log(err)
          this.setState({ validSenderbox: false, validIP: isValidIp })
        })
    } else {
      this.setState({ validSenderbox: false, validIP: isValidIp })
    }
  } // if its mid request and a proper ip is entered later on, has possibility of returning errors as it will say its no longer a valid ip due to request time

  render () {
    return (
      <React.Fragment>
        <Header>
          <h1>Senderbox list</h1>
        </Header>
        <Container>
          {this.state.senderboxes ? (
            <Card>
              {this.state.senderboxes.map((senderbox, i) => {
                return <h2 key={i}>{senderbox.name}</h2>
              })}
            </Card>
          ) : null}
        </Container>
      </React.Fragment>
    )
  }
}

// AddPi.propTypes = {

// }

// `new RegExp('[^@]+@[^@]+\.[^@]+')`
// new RegExp('[^@]+@[^@]+\\.[^@]+')
