// REFERENCES: Folowed tutorial by Ayooluwa Isaiah from Pusher (https://pusher.com/tutorials/customer-service-chat-react)
// ACCESSED: 27/03/2019
// Made changes to styling, file names, deleted dialog file and functions, Client chat loads on page load, client name is set at dev time
   import React, { Component } from "react";
   import axios from "axios";
   import Chatkit from "@pusher/chatkit-client";
   import { sendMessage, handleInput, connectToRoom } from "../sharedMethods";

   import "../tech-support.sass";

   class TechSide extends Component {
     constructor() {
       super();
       this.state = {
         currentUser: null,
         currentRoom: null,
         newMessage: "",
         messages: [],
         rooms: []
       };

       this.sendMessage = sendMessage.bind(this);
       this.handleInput = handleInput.bind(this);
       this.connectToRoom = connectToRoom.bind(this);
       this.joinRoom = this.joinRoom.bind(this);


     }

// Once user is selected, connects to that chat room and loads previous messages
     joinRoom(id) {
        this.setState(
          {
            messages: []
          },
          () => this.connectToRoom(id)
        );
      };

// On page load, connects to the server and opens the chatroom using chatKit
componentDidMount() {
  // Changes browser tab name
  document.title = "Videobrix Tech Support";
   const userId = "support";

   axios
     .post("http://localhost:3000/users", { userId })
     .then(() => {
       const tokenProvider = new Chatkit.TokenProvider({
         url: "http://localhost:3000/authenticate"
       });

       const chatManager = new Chatkit.ChatManager({
         instanceLocator: "v1:us1:530b1432-3df4-4a7d-a42f-a2c592e90264",
         userId,
         tokenProvider
       });

       return chatManager
         .connect({
           onAddedToRoom: room => {
             this.setState({
               rooms: [...this.state.rooms, room]
             });
           }
         })
         .then(currentUser => {
           this.setState(
             {
               currentUser,
               rooms: currentUser.rooms
             },
             () => {
               if (this.state.rooms.length >= 1) {
                 this.connectToRoom(this.state.rooms[0].id);
               }
             }
           );
         });
     })
     .catch(console.error);
 }


     render() {
       const {
         newMessage,
         rooms,
         currentRoom,
         messages,
         currentUser
       } = this.state;

       const RoomList = rooms.map(room => {
         // Changes display of selected room on the sidebar
         const isActive =
           currentRoom && currentRoom.id === room.id ? "active" : "";
         return (
           <li
             key={room.id}
             onClick={() => this.joinRoom(room.id)}
             className={`${isActive} room`}
           >
             {room.name}
           </li>
         );
       });

// Loads messages belonging to the chatroom
       const ChatSession = messages.map((message, index) => {
         const user = message.senderId === currentUser.id ? "support" : "user";
         return (
           <span key={index} className={`${user} message`}>
             {message.text}
           </span>
         );
       });

       return (
         <div className="tech-support">
         <div className="support-area">
           <aside className="support-sidebar">
             <h3>VIDEOBRIX CLIENTS</h3>
             <ul>{RoomList}</ul>
           </aside>
           <section className="support-session">
             <header className="current-chat">
               <h3>{currentRoom ? currentRoom.name : "Chat"}</h3>
             </header>
             <div className="chat-session">{ChatSession}</div>
             <form onSubmit={this.sendMessage} className="message-form">
               <input
                 className="message-input"
                 autoFocus
                 placeholder="How can we help?"
                 onChange={this.handleInput}
                 value={newMessage}
                 name="newMessage"
               />
             </form>
           </section>
         </div>
         </div>
       );
     }
   }

export default TechSide;
