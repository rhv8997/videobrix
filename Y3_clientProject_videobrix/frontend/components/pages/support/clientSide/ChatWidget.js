// REFERENCES: Folowed tutorial by Ayooluwa Isaiah from Pusher (https://pusher.com/tutorials/customer-service-chat-react)
// ACCESSED: 27/03/2019
// Made changes to styling, file names, deleted dialog file and functions, Client chat loads on page load, client name is set at dev time


    import React from "react";
    import Proptypes from "prop-types";

    const ChatWidget = props => {
      const { newMessage, sendMessage, handleInput, currentUser, messages } = props;

      // Gets the messages for the chat and creates the session using the sender id and the message
      const ChatSession = messages.map(message => {
        const user = message.senderId === currentUser.id ? "user" : "support";
        return <span className={`${user} message`}>{message.text}</span>;
      });
      // On form submit, it passes the message to the sendMessage function in the sharedFunction file to handle the message

      return (
        <section className="chat">
          <div className="chat-widget">
            <section className="chat-body">{ChatSession}</section>

            <form onSubmit={sendMessage} className="message-form">
              <input
                className="message-input"
                autoFocus
                name="newMessage"
                placeholder="How can we help?"
                onChange={handleInput}
                value={newMessage}

              />

            </form>
          </div>
        </section>
      );
    };

    // Required properties of text message
    ChatWidget.proptypes = {
      newMessage: Proptypes.string.isRequired,
      handleInput: Proptypes.func.isRequired,
      sendMessage: Proptypes.func.isRequired,
      messages: Proptypes.arrayOf(Proptypes.object).isRequired,
      currentUser: Proptypes.object.isRequired
    };

    export default ChatWidget;
