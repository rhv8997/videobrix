// REFERENCES: Folowed tutorial by Ayooluwa Isaiah from Pusher (https://pusher.com/tutorials/customer-service-chat-react)
// ACCESSED: 27/03/2019
// Made changes to styling, file names, deleted dialog file and functions, Client chat loads on page load, client name is set at dev time
import React, { Component } from 'react';
import Header from "../../layout/Header";
import Container from "../../layout/Container";
import axios from "axios";
import Chatkit from "@pusher/chatkit-client";
import ChatWidget from "./ChatWidget";
import { handleInput, sendMessage, connectToRoom } from "../sharedMethods";


import "../client-support.sass";

class ClientSide extends React.Component {

	constructor() {
			 super();

			 this.state = {
				 currentUser: null,
				 currentRoom: null,
				 newMessage: "",
				 messages: [],
				 userId: "",
				 _isMounted: false
			 };

			 this.connectToRoom = connectToRoom.bind(this);
			 this.sendMessage = sendMessage.bind(this);
			 this.handleInput = handleInput.bind(this);
			 this.addSupportStaffToRoom = this.addSupportStaffToRoom.bind(this);
			 this.createRoom = this.createRoom.bind(this);



		 }
// On page load, opens the chat room instantly and connects to server
		 componentDidMount(){
			 this._isMounted = true;

// TODO: make it not refresh messages on page load and use users username
			 const userId = "Client";

			if (userId === null || userId.trim() === "") {
				alert("Invalid userId");
			} else {
				axios
					.post("http://localhost:3000/users", { userId })
					.then(() => {
						const tokenProvider = new Chatkit.TokenProvider({
							url: "http://localhost:3000/authenticate"
						});

						const chatManager = new Chatkit.ChatManager({
							// Unique key from pusher account/instance/console

							instanceLocator: "v1:us1:530b1432-3df4-4a7d-a42f-a2c592e90264",
							userId,
							tokenProvider
						});

						return chatManager.connect().then(currentUser => {
							this.setState(
								{
									currentUser
								},
								() => this.createRoom()
							);
						});
					})
					.catch(console.error);
			}
		 }

// Adds tech support to the chatroom
		 addSupportStaffToRoom() {
			 const { currentRoom, currentUser } = this.state;

			 return currentUser.addUserToRoom({
				 userId: "support",
				 roomId: currentRoom.id
			 });
		 };

// On page load, creates a new room and adds it to chatkit pusher account
// Tech support get a new tab on their sidebar
		 createRoom() {
			 const { currentUser } = this.state;

			 currentUser
				 .createRoom({
					 name: currentUser.name,
					 private: true
				 })
				 .then(room => this.connectToRoom(room.id))
				 .then(() => this.addSupportStaffToRoom())
				 .catch(console.error);
		 };

		 componentWillUnmount() {
			 this._isMounted = false;
		 }







		 render() {
			 const {
				 newMessage,
				 messages,
				 currentUser,
				 currentRoom,
				 userId
			 } = this.state;

			 return (
			<React.Fragment>
				<Header/>
				<Container>
				<div className="support">
				<div className="customer-chat">

	              <ChatWidget
	                newMessage={newMessage}
	                sendMessage={this.sendMessage}
	                handleInput={this.handleInput}
	                currentUser={currentUser}
	                messages={messages}
									/>



	          </div>

				</div>
				</Container>

			</React.Fragment>
		);
	}
}

export default ClientSide;
