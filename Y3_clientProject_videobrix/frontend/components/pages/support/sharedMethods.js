// REFERENCES: Folowed tutorial by Ayooluwa Isaiah from Pusher (https://pusher.com/tutorials/customer-service-chat-react)
// ACCESSED: 27/03/2019
// Made changes to styling, file names, deleted dialog file and functions, Client chat loads on page load, client name is set at dev time


// Connects the message sent to the sender
function handleInput(event) {
      const { value, name } = event.target;

      this.setState({
        [name]: value
      });
    }
// gets the room, sender and message sent
    function sendMessage(event) {
      event.preventDefault();
      const { newMessage, currentUser, currentRoom } = this.state;

      if (newMessage.trim() === "") return;

      currentUser.sendMessage({
        text: newMessage,
        roomId: `${currentRoom.id}`
      });

      this.setState({
        newMessage: ""
      });
    }


// Sets up chatroom connection between the tech support and the user
    function connectToRoom(id) {
      const { currentUser } = this.state;

      return currentUser
        .subscribeToRoom({
          roomId: `${id}`,
          messageLimit: 100,
          hooks: {
            onMessage: message => {
              this.setState({
                messages: [...this.state.messages, message]
              });
            },
          }
        })
        .then(currentRoom => {
          this.setState({
            currentRoom
          });
        });
    }

    export { handleInput, sendMessage, connectToRoom };
