// Imports
import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'

export default class Video extends React.Component {
  constructor (props) {
    super(props)
    this.removeVideo = this.removeVideo.bind(this)
  }
  removeVideo () {
    const { playlistId, videoId } = this.props
    const token = localStorage.getItem('jwt')
    axios.get(`/api/playlist/${playlistId}/remove/${videoId}`, { headers: { Authorization: 'Bearer ' + token } }).then(resp => {
      if (resp.data.status) {
        this.props.removeVideo(videoId)
      } else {
        console.log(`failed to remove err: ${resp.data.message}`)
      }
    })
  }
  render () {
    return (
      <React.Fragment>
        <div className='video'>
          <div className='image' style={{ backgroundImage: `url(${this.props.image})` }} />
          <div className='content'>
            <h2>{this.props.name}</h2>
            <p>{this.props.description}</p>
            <button onClick={this.removeVideo} className='red'>Remove from playlist</button>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

Video.propTypes = {
  playlistId: PropTypes.string,
  videoId: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  removeVideo: PropTypes.func
}
