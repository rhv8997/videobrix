// Imports
import React from 'react'
import axios from 'axios'
import jwt from 'jsonwebtoken'

// React Components
import Header from '../layout/Header'
import Container from '../layout/Container'
import TextBox from '../../forms/TextBox'
import TextArea from '../../forms/TextArea'
import Card from '../../generic/Card'
import Submit from '../../forms/Submit'
import Form from '../../forms/Form'
import Select from '../../forms/Select'

import history from '../../../src/history'

// Style
import './playlists.sass'

import LocationContext from '../../locationContext'
import FileUpload from '../../forms/FileUpload'
export default class CreatePlaylist extends React.Component {
  constructor (props) {
    super(props)
    this.state = { playlist: null, videos: [] }
    this.oldContext = null
    this.createPlaylist = this.createPlaylist.bind(this)
  }
  componentDidMount () {
    let token = localStorage.getItem('senderboxjwt')
    let jwtdata = jwt.decode(token)
    this.currentpi = jwtdata.ip
    console.log("hehe")
    axios.get(`http://${this.currentpi}/api/allVideos`).then(resp => {
      console.log(resp)
      console.log('hehe')
      return resp.data.map(video => {
        return { actualValue: video[1], shownValue: video[0] }
      })
    }).then(videos => {
      console.log('hehe')
      this.setState({ videos })
    })
  }
  createPlaylist (data) {
    let form = new FormData()
    form.append('name', data.name)
    form.append('image', data.image)
    form.append('description', data.description)
    data.videos.forEach(video => form.append('videos', video))
    axios.post(`http://${this.currentpi}/api/playlists/create`, form, { headers: { 'Content-Type': 'multipart/form-data' } }).then(resp => {
      if (resp.data.status) {
        history.push(`/playlist/${resp.data.playlist_id}`)
      }
    })
    console.log(form)
  }
  render () {
    const { videos } = this.state
    return (
      <React.Fragment>
        <Header>
          <h1>Create playlist</h1>
        </Header>
        <Container>
          <Card >
            {videos.length !== 0
              ? <Form onSubmit={this.createPlaylist}>
                <TextBox name='name' title='Playlist name' placeholder='Playlist name' required />
                <TextArea name='description' title='Playlist description' placeholder='Playlist description' required />
                <FileUpload title='Playlist Image' name='image' />
                <Select name='videos' title='Playlist videos' multiple placeholder='Playlist name' options={videos} />
                <Submit />
              </Form>
              : null }
          </Card>
        </Container>
      </React.Fragment>
    )
  }
}

CreatePlaylist.contextType = LocationContext

// a
