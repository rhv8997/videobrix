// Imports
import React from 'react'
import PropTypes from 'prop-types'
import jwt from 'jsonwebtoken'

// React Components
import { Link } from 'react-router-dom'

export default class PlaylistCard extends React.Component {
  render () {
    let token = localStorage.getItem('senderboxjwt')
    if (!token) { this.setState({ errors: { message: 'Not connected to a senderbox' } }); return false }
    let jwtdata = jwt.decode(token)
    this.currentpi = jwtdata.ip
    console.log(this.currentpi)
    return (
      <div className='thin-card playlist'>
        <div className='padding'>
          <h1>{this.props.title}</h1>
          <p>{this.props.description}</p>
        </div>
        <div className='image' style={{ backgroundImage: `url('http://${this.currentpi}/static/images/${this.props.image}')` }} />
        <div className='padding actions'>
          <Link to={`playlist/${this.props.id}`}>View</Link>
        </div>
      </div>
    )
  }
}

PlaylistCard.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  id: PropTypes.string
}
