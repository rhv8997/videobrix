// Imports
import React from 'react'
import axios from 'axios'
import jwt from 'jsonwebtoken'

// React Components
import Header from '../layout/Header'
import Container from '../layout/Container'
import ThinCard from '../../generic/ThinCard'
import PlaylistCard from './PlaylistCard'
import history from '../../../src/history'

// Style
import './playlists.sass'

export default class Playlists extends React.Component {
  constructor (props) {
    super(props)
    this.state = { playlists: null }
    this.createPlaylist = React.createRef()
  }
  componentWillMount () {
    let token = localStorage.getItem('senderboxjwt')
    if (!token) { this.setState({ errors: { message: 'Not connected to a senderbox' } }); return false }
    let jwtdata = jwt.decode(token)
    this.currentpi = jwtdata.ip
    axios
      .get(`http://${this.currentpi}/api/playlists`, { headers: { Authorization: 'Bearer ' + token } })
      .then(resp => {
        this.setState({ playlists: resp.data.playlists })
      })
  }
  render () {
    return (
      <React.Fragment>
        <Header>
          <h1>Playlists</h1>
        </Header>
        <Container>
          <div className='playlists'>
            {this.state.playlists
              ? this.state.playlists.map((playlist, i) => {
                return (
                  <PlaylistCard
                    key={i}
                    id={playlist.id}
                    title={playlist.name}
                    image={playlist.image}
                    description={playlist.description}
                  />
                )
							  })
              : null}
              <ThinCard class='playlist'>
                <div className='padding' onClick={() => {history.push('/playlist/create')}}>
                  <h1>Click here to create a playlist</h1>
                </div>
              </ThinCard>
          </div>
        </Container>
      </React.Fragment>
    )
  }
}
