// Imports
import React from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import jwt from 'jsonwebtoken'

// React Components
import Header from '../layout/Header'
import Container from '../layout/Container'
import ThinCard from '../../generic/ThinCard'
import Video from './Video'

// Style
import './playlists.sass'

export default class Playlist extends React.Component {
  constructor (props) {
    super(props)
    this.state = { playlist: null, videos: null }
    this.removeVideo = this.removeVideo.bind(this)
  }
  componentWillMount () {
    let token = localStorage.getItem('senderboxjwt')
    if (!token) { this.setState({ errors: { message: 'Not connected to a senderbox' } }); return false }
    let jwtdata = jwt.decode(token)
    this.currentpi = jwtdata.ip
    const { id } = this.props.match.params
    console.log(id)
    console.log(`http://${this.currentpi}/api/playlists/${id}`)
    axios.get(`http://${this.currentpi}/api/playlists/${id}`, { headers: { Authorization: 'Bearer ' + token } }).then(resp => {
      if (resp.data.status) {
        this.setState({ playlist: resp.data.playlist })
      } else {
        this.setState({ error: resp.data.message })
      }
    })
    axios.get(`http://${this.currentpi}/api/playlists/${id}/videos`, { headers: { Authorization: 'Bearer ' + token } }).then(resp => {
      if (resp.data.status) {
        this.setState({ videos: resp.data.videos })
      } else {
        this.setState({ error: resp.data.message })
      }
    })
  }
  removeVideo (id) {
    console.log(id)
    let playlist = Object.assign({}, this.state.playlist)
    playlist.videos = playlist.videos.filter(video => video._id !== id)
    console.log(playlist)
    this.setState({ playlist: playlist })
  }
  render () {
    return (
      <React.Fragment>
        <Header>
          <h1>Playlist</h1>
        </Header>
        <Container>
          {this.state.playlist && this.state.videos
            ? <React.Fragment>
              <ThinCard class='playlist-header'>
                <div className='padding'>
                  <h1>{this.state.playlist.name}</h1>
                  <p>{this.state.playlist.description}</p>
                </div>
                <div className='image' style={{ backgroundImage: `url('http://${this.currentpi}/static/images//${this.state.playlist.image}')` }} />
              </ThinCard>
              <ThinCard class='playlist-videos'>
                <div className='padding'>
                  <h1>Videos</h1>
                </div>
                <div className='video-list'>
                  {this.state.videos.map((video, i) => {
                    console.log(video)
                    return (
                      <Video
                        key={i}
                        videoId={video._id}
                        playlistId={this.state.playlist._id}
                        name={video.name}
                        image={`http://${this.currentpi}/static/images/${video.file}`}
                        description={video.description}
                        removeVideo={this.removeVideo} />)
                  })}
                </div>
              </ThinCard>
              
            </React.Fragment>
            : null}
          {this.state.error
            ? <div>{this.state.error}</div>
            : null }
        </Container>
      </React.Fragment>
    )
  }
}

Playlist.propTypes = {
  match: PropTypes.object
}
