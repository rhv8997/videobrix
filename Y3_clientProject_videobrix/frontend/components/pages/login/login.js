// Imports
import React from 'react'
import axios from 'axios'

// Local Functions
import history from '../../../src/history'

// React Components
import Form from '../../forms/Form'
import Textbox from '../../forms/TextBox'
import Submit from '../../forms/Submit'
import Tick from '../../animations/Tick'
import Loading from '../../animations/Loading'

// Style
import './login.sass'

export default class Login extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      authenticating: false,
      auth: null
    }
    this.onSubmit = this.onSubmit.bind(this)
  }
  onSubmit (formData) {
    this.setState({ authenticating: true })
    axios.post('/api/login', { username: formData.username, password: formData.password }).then(response => {
      if (response.data.auth) {
        this.setState({ auth: true }, () => {
          setTimeout(() => {
            localStorage.setItem('jwt', response.data.token)
            history.push('/dashboard')
          }, 1000) // wait for the tick to be finished lol gimmicky, remove it at some point
        })
      } else {
        this.setState({ authenticating: false })
      }
    })
  }
  render () {
    return (
      <div className='login-wrapper' >
        <h1>Videobrix</h1>
        <h2>Login</h2>
        <div className='login-container card'>
          {this.state.auth
            ? <Tick />
            : <React.Fragment>
              {this.state.authenticating
                ? <Loading />
                : <Form onSubmit={this.onSubmit}>
                  <Textbox title='Username' name='username' required />
                  <Textbox title='Password' name='password' type='password' required />
                  <Submit value='Login' />
                </Form>}
            </React.Fragment>
          }
        </div>
      </div>
    )
  }
}
