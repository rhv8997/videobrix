// Imports
import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import PropTypes from 'prop-types'

// React components
import Loading from '../../animations/Loading'

// Local code
import { authCheck } from '../../../../src/auth'

export default class PrivateRoute extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      auth: null,
      loading: true
    }
  }
  componentWillMount () {
    authCheck().then(response => {
      console.log(response)
      this.setState({
        loading: false,
        auth: response.data.auth
      })
    }).catch(error => {
      this.setState({
        loading: false,
        auth: false
      })
      console.log('Auth error: ' + error)
    })
  }
  render () {
    let Component = this.props.component
    const { computedMatch } = this.props
    return (
      <React.Fragment>
        {this.state.loading
          ? <Loading />
          : <Route
            render={props =>
              this.state.auth ? (
                <Component {...props} />
              ) : (
                <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
              )
            } computedMatch={computedMatch} />
        }
      </React.Fragment>
    )
  }
}

// Maybe dont have post request on every page load to see auth? seems a bit

PrivateRoute.propTypes = {
  component: PropTypes.func,
  computedMatch: PropTypes.object
}
