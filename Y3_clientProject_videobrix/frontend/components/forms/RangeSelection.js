import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './RangeSelection.sass'

class RangeSelection extends Component {
  constructor (props) {
    super(props)

    // Binding to satisfy Javascript.
    this.handleChange = this.handleChange.bind(this)
  }

  // Handling value change by using a callback function.
  handleChange (event) {
    this.props.onChangeData(event.target.value)
  }

  // Renders a slider input according to props provided by the
  // creator of the component.
  // if no values are given it uses default values to create
  // a slider.
  // REFERNCE: How to create a slider and style W3Schools
  // https://www.w3schools.com/howto/howto_js_rangeslider.asp
  // Last accessed on 21/03/2019
  render () {
    return (
      <input type='range' className='slider' name={this.props.name}
        value={this.props.defaultValue}
        min={this.props.min}
        max={this.props.max}
        onChange={this.handleChange} />
    )
  }
}

RangeSelection.propTypes = {
  name: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  onChangeData: PropTypes.func,
  defaultValue: PropTypes.number
}

RangeSelection.defaultProps = {
  name: 'Range',
  defaultValue: 50,
  min: 0,
  max: 100
}

export default RangeSelection
