// REF: TESTS ARE BASED ON:
// https://airbnb.io/enzyme/docs/api/ReactWrapper/get.html
// https://airbnb.io/enzyme/docs/api/ReactWrapper/find.html
// https://stackoverflow.com/questions/42330131/jest-js-tests-dont-pass-when-expected-received-values-are-objects
import React from 'react'
import { shallow } from 'enzyme'

// import { shallow, mount, render } from 'enzyme' old
import SettingsButton from './SettingsButton.js'

it('Creates a settings button with default values.', () => {
  const wrapper = shallow(<SettingsButton />)
  expect(wrapper.html()).toEqual('<button class="settings-button">A Button</button>')
})

it('Creates a settings button with custom name.', () => {
  const wrapper = shallow(<SettingsButton name="Test Button"/>)
  expect(wrapper.html()).toEqual('<button class="settings-button">Test Button</button>')
})
// End of referenced code.
