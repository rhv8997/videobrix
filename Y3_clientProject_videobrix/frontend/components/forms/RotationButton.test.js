// REF: TESTS ARE BASED ON:
// https://airbnb.io/enzyme/docs/api/ReactWrapper/get.html
// https://airbnb.io/enzyme/docs/api/ReactWrapper/find.html
// https://stackoverflow.com/questions/42330131/jest-js-tests-dont-pass-when-expected-received-values-are-objects
import React from 'react'
import { shallow } from 'enzyme'

// import { shallow, mount, render } from 'enzyme' old
import RotationButton from './RotationButton.js'

it('Creates a rotation button.', () => {
  const wrapper = shallow(<RotationButton />)
  expect(wrapper.html()).toEqual('<button type="button" name="button" class="rotation-button"></button>')
})

// End of referenced code.
