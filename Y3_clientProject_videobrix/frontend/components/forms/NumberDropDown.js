import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './NumberDropDown.sass'

// Global reference for logic used for using states to submit form data.
// https://reactjs.org/docs/forms.html LAST ACCESSED ON 21/03/2019

//  REF:Props and components
// https://reactjs.org/docs/components-and-props.html LAST ACCESSED ON 21/03/2019

//  REF: States and lifecycle.
// https://reactjs.org/docs/state-and-lifecycle.html LAST ACCESSED ON 21/03/2019
class NumberDropDown extends Component {
  constructor (props) {
    super(props)

    // List of numbers that can be selected for input as default state.
    this.state = {
      list: [1]
    }

    // Binding to satisfy Javascript.
    this.handleChange = this.handleChange.bind(this)
  }

  // Handling number change by using a callback function.
  handleChange (event) {
    this.props.onChangeData(event.target.value)
  }

  // Use of a for loop to create a new list of numbers
  // that will be used to create new list of available options.
  // this occours before component mounts so
  // user is displayed a correct list on page load.
  // min and max value is defined by developer by props.
  componentWillMount () {
    var newList = []
    for (var i = this.props.min; i < this.props.max; i++) {
      if (i <= 9) {
        newList.push('0' + String(i))
      } else {
        newList.push(String(i))
      }
    }

    // After creation of the new list set state is used to replace the
    // old list.
    this.setState({ list: newList })
  }

  // Render method to create something that end user will see
  // Uses logic created above to render.
  // REF: Why map is used to create a list of components.
  // https://reactjs.org/docs/lists-and-keys.html [Last Accessed on 21/03/2019]
  render () {
    return (
      <select className='drop-down' onChange={this.handleChange}>
        {
          this.state.list.map((info) => {
            return (
              <option value={info} key={info}>{info}</option>
            )
          })
        }
      </select>
    )
  }
}

NumberDropDown.propTypes = {
  onChangeData: PropTypes.func,
  min: PropTypes.number,
  max: PropTypes.number
}

NumberDropDown.defaultProps = {
  min: 0,
  max: 24
}

export default NumberDropDown
