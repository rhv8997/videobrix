// Imports
import React from 'react'
import PropTypes from 'prop-types'

// Local Functions
import Validator from '../../src/validator'

// Style
import './form.sass'

// React Components
import Errors from './Errors'

export default class TextBox extends React.Component {
  constructor (props) {
    super(props)
    this.state = { errors: null, value: '' }

    this.typing = false
    this.typingTimer = null
    this.afterTimer = null

    this.props.updateInput(props.name, '')
    this.validator = new Validator(props)
    this.validator.validateInput(this.state.value).then(errors => {
      let problems = errors.filter(error => !error.passed)
      props.setErrors(props.name, problems)
    })

    this.handleChange = this.handleChange.bind(this)
    this.checkErrors = this.checkErrors.bind(this)

    if (this.props.after && !this.props.do) {
      console.log('You have an after prop but no do prop, the after prop is redundant')
    }
    if (!this.props.after && this.props.do) {
      console.log('You have a do prop but no after prop, the do prop is redundant')
    }
  }
  handleChange (e) {
    if (!this.props.name) {
      console.warn("If you wish to return the data from a form, you must provide a name (i.e name='x' in the JSX element) for the script to use as a key")
      return false
    }
    let val = e.target.value
    this.setState({
      value: val
    }, () => {
      this.props.updateInput(this.props.name, val) // ERROR CATCH FOR SAME KEY
    })
    this.typing = true
    clearInterval(this.typingTimer)
    this.typingTimer = setTimeout(() => { this.typing = false; this.checkErrors() }, 600)
    if (this.props.after && this.props.do) {
      clearInterval(this.afterTimer)
      this.afterTimer = setTimeout(() => { this.props.do(this.state.value) }, this.props.after)
    }
  }
  componentWillUnmount () {
    clearInterval(this.typingTimer)
  }
  checkErrors () {
    return new Promise((resolve, reject) => {
      this.validator.validateInput(this.state.value).then(errors => {
        let problems = errors.filter(error => !error.passed)
        this.props.setErrors(this.props.name, problems)
        if (problems.length === 0) {
          this.setState({ errors: null }, resolve)
        } else {
          this.setState({ errors: problems }, resolve)
        }
      })
    })
  }
  render () {
    const { title, name, placeholder, value, type, customErrors } = this.props
    let { errors } = this.state
    if (customErrors) {
      let failed = customErrors.filter(error => error.passed === false)
      if (!errors && failed.length !== 0) {
        errors = failed
      } else if (failed.length !== 0) {
        errors.concat(failed)
      }
    }
    return (
      <div className='input-wrapper'>
        <div className={'input' + (errors ? ' error' : '')}>
          {title ? <h1>{title + (errors ? ' - Errors' : '')}</h1> : null}
          <input
            name={name || null}
            type={type || 'text'}
            placeholder={placeholder || null}
            defaultValue={value || null}
            onChange={this.handleChange}
            onBlur={this.checkErrors} />
        </div>
        {errors
          ? <Errors errors={errors} />
          : null }
      </div>
    )
  }
}

TextBox.propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  setErrors: PropTypes.func,
  type: PropTypes.string,
  updateInput: PropTypes.func,
  after: PropTypes.number,
  do: PropTypes.func,
  customErrors: PropTypes.array
}
