// REF: TESTS ARE BASED ON:
// https://airbnb.io/enzyme/docs/api/ReactWrapper/get.html
// https://airbnb.io/enzyme/docs/api/ReactWrapper/find.html
// https://stackoverflow.com/questions/42330131/jest-js-tests-dont-pass-when-expected-received-values-are-objects
import React from 'react'
import { shallow } from 'enzyme'

// import { shallow, mount, render } from 'enzyme' old
import DayDropDown from './DayDropDown.js'

it('Creates Day selection.', () => {
  const wrapper = shallow(<DayDropDown />)
  expect(wrapper.html()).toEqual('<select class="drop-down"><option value="Monday">Monday</option><option value="Tuesday">Tuesday</option><option value="Wednesday">Wednesday</option><option value="Thursday">Thursday</option><option value="Friday">Friday</option><option value="Saturday">Saturday</option><option value="Sunday">Sunday</option></select>')
})

// End of referenced code.
