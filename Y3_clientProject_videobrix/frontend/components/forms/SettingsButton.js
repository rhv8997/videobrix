import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './SettingsButton.sass'

export default class SettingsButton extends Component {
  // Very basic component that renders a button
  // that uses a callback function.
  // REFERENCE: Props and components
  // https://reactjs.org/docs/components-and-props.html
  render () {
    return (
      <button className='settings-button' onClick={this.props.clickAction}>{this.props.name}</button>
    )
  }
}

SettingsButton.propTypes = {
  name: PropTypes.string,
  clickAction: PropTypes.func
}

SettingsButton.defaultProps = {
  name: 'A Button'
}
