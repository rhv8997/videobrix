// REF: TESTS ARE BASED ON:
// https://airbnb.io/enzyme/docs/api/ReactWrapper/get.html
// https://airbnb.io/enzyme/docs/api/ReactWrapper/find.html
// https://stackoverflow.com/questions/42330131/jest-js-tests-dont-pass-when-expected-received-values-are-objects
import React from 'react'
import { shallow } from 'enzyme'

// import { shallow, mount, render } from 'enzyme' old
import RangeSelection from './RangeSelection.js'

it('Creates range selection.', () => {
  const wrapper = shallow(<RangeSelection />)
  expect(wrapper.html()).toEqual('<input type="range" class="slider" name="Range" value="50" min="0" max="100"/>')
})

it('Creates range selection Default Value.', () => {
  const wrapper = shallow(<RangeSelection defaultValue={5}/>)
  expect(wrapper.html()).toEqual('<input type="range" class="slider" name="Range" value="5" min="0" max="100"/>')
})

it('Creates range selection Minimum Value.', () => {
  const wrapper = shallow(<RangeSelection min={15}/>)
  expect(wrapper.html()).toEqual('<input type="range" class="slider" name="Range" value="50" min="15" max="100"/>')
})

it('Creates range selection Maximum Value.', () => {
  const wrapper = shallow(<RangeSelection max={20}/>)
  expect(wrapper.html()).toEqual('<input type="range" class="slider" name="Range" value="50" min="0" max="20"/>')
})
// End of referenced code.
