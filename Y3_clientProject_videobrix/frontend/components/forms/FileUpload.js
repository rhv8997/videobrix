// Imports
import React from 'react'
import PropTypes from 'prop-types'

// Style
import './form.sass'

// React Components
import Errors from './Errors'
import File from './File'

// value == files

class FileUpload extends React.Component {
  constructor (props) {
    super(props)
    this.state = { errors: null, files: [], uploaded: [] }

    this.fileInput = React.createRef()
    this.complete = false

    this.handleChange = this.handleChange.bind(this)
    this.browseFiles = this.browseFiles.bind(this)
    this.fileUploadBox = React.createRef()
  }
  browseFiles (e) {
    if (!e.target.classList.contains('select-file')) {
      return false
    }
    this.fileInput.current.click()
  }
  handleChange () {
    let colours = ['#2980b9', '#27ae60', '#16a085', '#d35400', '#c0392b']
    let actualFiles = this.props.append ? this.state.files : []
    let formatColours = {}
    for (let i in Object.keys(this.fileInput.current.files)) {
      if (formatColours[this.fileInput.current.files.item(i).type]) {
        actualFiles.push({ colour: formatColours[this.fileInput.current.files.item(i).type], file: this.fileInput.current.files.item(i) })
      } else {
        let colour = colours.splice(0, 1)[0]
        formatColours[this.fileInput.current.files.item(i).type] = colour
        actualFiles.push({ colour, file: this.fileInput.current.files.item(i) })
      }
    }
    let rawFiles = actualFiles.map(obj => obj.file)
    this.props.updateInput(this.props.name, this.props.multiple ? rawFiles : rawFiles[0])
    this.setState({ files: actualFiles, uploaded: [] })
  }
  checkErrors () {
    return new Promise((resolve, reject) => {
      resolve()
    })
  }
  render () {
    const { title, name, multiple, errors } = this.props
    const { files } = this.state
    return (
      <div className='input-wrapper'>
        <div className={'input' + (errors ? ' error' : '')}>
          {title ? <h1>{title + (errors ? ' - Errors' : '')}</h1> : null}
          <input
            ref={this.fileInput}
            name={name || null}
            type='file'
            onChange={this.handleChange}
            multiple />
          <div className={'select-file' + (multiple ? ' multiple' : '')} ref={this.fileUploadBox} onClick={this.browseFiles} >
            {files
              ? <React.Fragment>
                {files.map((file, i) => {
                  return (
                    <File
                      file={file.file}
                      colour={file.colour}
                      key={i}
                    />)
                })}
              </React.Fragment>
              : <div />}
          </div>
        </div>
        {errors
          ? <Errors errors={errors} />
          : null }
      </div>
    )
  }
}

export default FileUpload

FileUpload.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string,
  multiple: PropTypes.bool,
  append: PropTypes.bool,
  updateInput: PropTypes.func,
  errors: PropTypes.array
}

// a
