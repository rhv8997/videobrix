// Imports
import React from 'react'
import PropTypes from 'prop-types'

import './form.sass'

export default class TextArea extends React.Component {
  constructor (props) {
    super(props)
    this.state = { value: '', errors: [] }
    this.handleChange = this.handleChange.bind(this)
    this.props.updateInput(props.name, '')
  }
  handleChange (e) {
    if (!this.props.name) {
      console.warn("If you want to return the data from a form, you must provide a name (i.e name='x' in the JSX element) for the script to use as a key")
      return false
    }
    let val = e.target.value
    this.setState({
      value: val
    }, () => {
      this.props.updateInput(this.props.name, val) // ERROR CATCH FOR SAME KEY
    })
  }
  checkErrors () {
    return new Promise((resolve, reject) => {
      resolve()
    })
  }
  render () {
    const { title, name, placeholder } = this.props
    const { value } = this.state
    return (
      <div className='input-wrapper'>
        <div className='input'>
          <h1>{title}</h1>
          <textarea
            name={name || null}
            onChange={(e) => this.handleChange(e)}
            value={value || ''}
            placeholder={placeholder || null}
            style={{ resize: 'none' }}
          />
        </div>
      </div>
    )
  }
}

TextArea.propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  updateInput: PropTypes.func
}