import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './NumberDropDown.sass'

// Global reference for logic used for using states to submit form data.
// https://reactjs.org/docs/forms.html LAST ACCESSED ON 21/03/2019

//  REF:Props and components
// https://reactjs.org/docs/components-and-props.html LAST ACCESSED ON 21/03/2019

//  REF: States and lifecycle.
// https://reactjs.org/docs/state-and-lifecycle.html LAST ACCESSED ON 21/03/2019
class DayDropDown extends Component {
  constructor (props) {
    super(props)

    // List of days that can be selected for input.
    this.state = {
      list: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    }

    // Binding to satisfy Javascript.
    this.handleChange = this.handleChange.bind(this)
  }

  // Handling day change by using a callback function.
  handleChange (event) {
    this.props.onChangeData(event.target.value)
  }

  // REF: Why map is used to create a list of components.
  // https://reactjs.org/docs/lists-and-keys.html [Last Accessed on 21/03/2019]
  render () {
    return (
      <select className='drop-down' onChange={this.handleChange}>
        {
          this.state.list.map((info) => {
            return (
              <option value={info} key={info}>{info}</option>
            )
          })
        }
      </select>
    )
  }
}

DayDropDown.propTypes = {
  onChangeData: PropTypes.func
}

export default DayDropDown
