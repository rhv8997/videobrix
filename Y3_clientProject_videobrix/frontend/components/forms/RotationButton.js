import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './RotationButton.sass'

class RotationButton extends Component {
  // Very basic component that renders a button
  // that uses a callback function.
  // REFERENCE: Props and components
  // https://reactjs.org/docs/components-and-props.html
  render () {
    return (
      <button type='button' name='button' className='rotation-button' onClick={this.props.onRotationRequest} />
    )
  }
}

RotationButton.propTypes = {
  // name: PropTypes.string,
  onRotationRequest: PropTypes.func

}

RotationButton.defaultProps = {
  // name: 'rotation-button'

}

export default RotationButton
