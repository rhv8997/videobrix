// REF: TESTS ARE BASED ON:
// https://airbnb.io/enzyme/docs/api/ReactWrapper/get.html
// https://airbnb.io/enzyme/docs/api/ReactWrapper/find.html
// https://stackoverflow.com/questions/42330131/jest-js-tests-dont-pass-when-expected-received-values-are-objects
import React from 'react'
import { shallow } from 'enzyme'

// import { shallow, mount, render } from 'enzyme' old
import NumberDropDown from './NumberDropDown.js'

it('Creates number selection with default values.', () => {
  const wrapper = shallow(<NumberDropDown />)
  expect(wrapper.html()).toEqual('<select class="drop-down"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>')
})


it('Creates number selection with Max values.', () => {
  const wrapper = shallow(<NumberDropDown min={22}/>)
  expect(wrapper.html()).toEqual('<select class="drop-down"><option value="22">22</option><option value="23">23</option></select>')
})


it('Creates number selection with Min values.', () => {
  const wrapper = shallow(<NumberDropDown max={3}/>)
  expect(wrapper.html()).toEqual('<select class="drop-down"><option value="00">00</option><option value="01">01</option><option value="02">02</option></select>')
})
// End of referenced code.
