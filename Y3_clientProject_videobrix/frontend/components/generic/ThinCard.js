// Imports
import React from 'react'
import PropTypes from 'prop-types'

export default class ThinCard extends React.Component {
  render () {
    return (
      <div className={'thin-card ' + (this.props.class || '')}>
        {this.props.children}
      </div>
    )
  }
}

ThinCard.propTypes = {
  class: PropTypes.string,
  children: PropTypes.node
}
