// Imports
import React from 'react'

import Card from '../generic/Card'
import ReactDOM from 'react-dom'

// React Components
// import history from '../../../src/history' uncomment when used, commented to bypass lint

import './popup.sass'
import ThinCard from './ThinCard'

const main = document.getElementById('root')

export default class Popup extends React.Component {
  constructor (props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick (e) {
    e.preventDefault()
    this.props.callback()
  }
  render () {
    return ReactDOM.createPortal(
      <div className='popup' style={{ top: this.props.y, left: this.props.x }}>
        <Card>
          <h1>{this.props.title}</h1>
          <form onSubmit={this.handleClick} className='video-delete'>
            <table className='delete-table'>
              <tbody>
                <tr>
                  <td>
                    <div className='delete-wrapper'>
                      <input
                        className='btn'
                        type='submit'
                        value='Yes'
                        className='yes-button'
                      />
                    </div>
                  </td>
                  <td>
                    <button className='no-button' onClick={this.props.closePopup}>
											No
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </form>
        </Card>
      </div>,
      main
    )
  }
}
