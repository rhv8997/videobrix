// Imports
import React from 'react'
import PropTypes from 'prop-types'

export default class Card extends React.Component {
  render () {
    return (
      <div className={'card ' + (this.props.class || '')}>
        {this.props.children}
      </div>
    )
  }
}

Card.propTypes = {
  class: PropTypes.string,
  children: PropTypes.node
}
