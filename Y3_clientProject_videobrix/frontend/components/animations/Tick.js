// Imports
import React from 'react'

// Style
import './animations.sass'

export default class Tick extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      draw: false
    }
    this.tick = React.createRef()
  }
  componentDidMount () {
    setTimeout(() => {
      this.setState({ draw: true })
    }, 50)
  }
  render () {
    return (
      <React.Fragment>
        <div className={'trigger' + (this.state.draw ? ' drawn' : '')} ref={this.tick} />
        <svg version='1.1' id='tick' xmlns='http://www.w3.org/2000/svg' xmlnsXlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 37 37' style={{ enableBackground: 'new 0 0 37 37', height: '100px' }} xmlSpace='preserve'>
          <path className='circ path' style={{ fill: 'none', stroke: '#27ae60', strokeWidth: 3, strokeLinejoin: 'round', strokeMiterlimit: 10 }} d='
            M30.5,6.5L30.5,6.5c6.6,6.6,6.6,17.4,0,24l0,0c-6.6,6.6-17.4,6.6-24,0l0,0c-6.6-6.6-6.6-17.4,0-24l0,0C13.1-0.2,23.9-0.2,30.5,6.5z' />
          <polyline className='tick path' style={{ fill: 'none', stroke: '#2ecc71', strokeWidth: 3, strokeLinejoin: 'round', strokeMiterlimit: 10 }} points='
            11.6,20 15.9,24.2 26.4,13.8 ' />
        </svg>
      </React.Fragment>

    )
  }
}

// https://codepen.io/houbly/pen/yyzajr - Thanks to this dude
