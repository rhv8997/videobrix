// Imports
import React from 'react'

// Style
import './animations.sass'

export default class Loading extends React.Component {
  render () {
    return (
      <div className='lds-ring'><div /><div /><div /><div /></div>

    )
  }
}

// https://loading.io/
