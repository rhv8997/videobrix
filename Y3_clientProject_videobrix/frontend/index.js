// React
import React from 'react'
import { render } from 'react-dom'
import App from './components/app'

import history from './src/history'
import { Router } from 'react-router-dom'

// Render
render(
  <Router history={history}>
    <App />
  </Router>,
  document.getElementById('root')
)
