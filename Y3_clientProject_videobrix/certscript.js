const rsa = require('node-rsa')
const fs =  require('fs')
const path = require('path')

console.log('creating 2048 bit key, may take a while, please be patient')
const key = new rsa({b: 2048});
const certDir = path.join(__dirname, '/backend' ,'/certs')



const makeDir = (dir) => {
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
}


console.log('creating directory')
makeDir(certDir)
console.log('creating keys')
fs.writeFileSync(path.join(certDir, 'key.pem'), key.exportKey('pkcs8'))
fs.writeFileSync(path.join(certDir, 'cert.pem'), key.exportKey('pkcs8-public-pem'))
console.log('Finished')